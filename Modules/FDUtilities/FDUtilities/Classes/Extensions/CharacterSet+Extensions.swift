//
//  CharacterSet+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 09.07.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import Foundation

public extension CharacterSet {
	public static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}
