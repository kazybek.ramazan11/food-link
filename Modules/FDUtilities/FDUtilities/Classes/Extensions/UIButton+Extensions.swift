//
//  UIButton+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 13.07.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import FDUIKit

extension UIButton {
    public static func primaryButton(title: String, target: Any, action: Selector) -> UIButton {
        let button = UIButton.primaryButton(title: title)
		button.tap(target: target, selector: action)
        return button
    }
//    
	public static func primaryButton(title: String, color: UIColor = .white, disabledColor: UIColor = .white, background: UIColor = .appOrange, disabledBackground: UIColor = .appGray, cornerRadius: CGFloat = 7, font: UIFont = FontFamily.Roboto.bold.font(size: 17)) -> UIButton {
		let button = UIButton(type: .custom)
        button.setTitle(title, for: .normal)
        button.setTitleColor(color, for: .normal)
        button.setTitleColor(disabledColor, for: .disabled)
//		button.setBackgroundImage(background.as1ptImage(), for: .normal)
//		button.setBackgroundImage(UIColor.disabledGray.as1ptImage(), for: .disabled)
		button.backgroundColor = .appOrange
		button.corner(radius: 7)
        button.clipsToBounds = true
        button.titleLabel?.font = font
        return button
    }
//    
//    static func brightButton(title: String, target: Any, action: Selector) -> UIButton {
//        let button = UIButton.brightButton(title: title)
//        button.addTarget(target, action: action, for: .touchUpInside)
//        return button
//    }
//    
//    static func brightButton(title: String, color: UIColor = .appBlue, disabledColor: UIColor = .white, background: UIColor = .secondaryBlue, disabledBackground: UIColor = .darkGray, cornerRadius: CGFloat = 4, font: UIFont = FontFamily.Roboto.regular.font(size: 14)) -> UIButton {
//        let button = UIButton(type: .system)
//        button.setTitle(title, for: .normal)
//        button.setTitleColor(color, for: .normal)
//        button.setBackgroundImage(background.as1ptImage(), for: .normal)
//        button.setBackgroundImage(disabledBackground.as1ptImage(), for: .disabled)
//        button.setTitleColor(disabledColor, for: .disabled)
//        button.layer.cornerRadius = cornerRadius
//        button.clipsToBounds = true
//        button.titleLabel?.font = font
//        return button
//    }
//    
//    static func brightButton(image: UIImage, inset: CGFloat = 7, background: UIColor = .secondaryBlue, disabledBackground: UIColor = .darkGray, cornerRadius: CGFloat = 4) -> UIButton {
//        let button = UIButton(type: .system)
//        button.setImage(image, for: .normal)
//        button.setBackgroundImage(disabledBackground.as1ptImage(), for: .disabled)
//        button.setBackgroundImage(background.as1ptImage(), for: .normal)
//        button.layer.cornerRadius = cornerRadius
//        button.setInsets(horizontal: inset, vertical: inset)
//        button.clipsToBounds = true
//        return button
//    }
//    
//    static func underlinedButton(title: String, target: Any, action: Selector) -> UIButton {
//        let button = UIButton.underlinedButton(title: title)
//        button.addTarget(target, action: action, for: .touchUpInside)
//        return button
//    }
//    
//    static func underlinedButton(title: String,
//                               color: UIColor = .appOrange,
//                               tintColor: UIColor = .linkBlue,
//                               background: UIColor = .white) -> UIButton {
//        let button = UIButton.linkButton(title: "", color: color, tintColor: tintColor, background: background)
//        let attributedText = NSMutableAttributedString(string: title, attributes: [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue])
//        button.setAttributedTitle(attributedText, for: .normal)
//        return button
//    }
//    
//    static func linkButton(title: String, target: Any, action: Selector) -> UIButton {
//        let button = UIButton.linkButton(title: title)
//        button.addTarget(target, action: action, for: .touchUpInside)
//        return button
//    }
//    
//    static func linkButton(title: String,
//                               color: UIColor = .appOrange,
//                               tintColor: UIColor = .linkBlue,
//                               background: UIColor = .white) -> UIButton {
//        let button = UIButton(type: .system)
//        button.setTitle(title, for: .normal)
//        button.tintColor = tintColor
//        button.layer.borderWidth = 0
//        button.setTitleColor(tintColor, for: .normal)
//        button.setTitleColor(.linkBlue, for: .disabled)
//        button.setBackgroundImage(background.as1ptImage(), for: .normal)
//        button.titleLabel?.numberOfLines = 0
//        button.titleLabel?.font = FontFamily.Roboto.regular.font(size: 14)
//        return button
//    }
//    
    public func underline() {
        guard let text = self.titleLabel?.text else { return }
        let attributedString = NSMutableAttributedString(string: text)
        //NSAttributedStringKey.foregroundColor : UIColor.blue
        attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
        self.setAttributedTitle(attributedString, for: .normal)
    }
    
    public func underline(text: String) {
        let attributedString = NSMutableAttributedString(string: text)
        //NSAttributedStringKey.foregroundColor : UIColor.blue
        attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
        self.setAttributedTitle(attributedString, for: .normal)
	}
////
////    static func selectableButton(title: String, corner: CGFloat = 6) -> UIButton {
////        let button = UIButton.borderedButton(title: title, color: .lavender, tintColor: .appBlack, corner: corner)
////        button.setBackgroundImage(UIColor.appOrange.as1ptImage(), for: .selected)
////        button.setTitleColor(.white, for: .selected)
////        button.titleLabel?.font = FontFamily.Roboto.regular.font(size: 14)
////        button.clipsToBounds = true
////        return button
////    }
//    
    // MARK: Actions

	public func tap(target: Any?, selector: Selector) {
        self.addTarget(target, action: selector, for: .touchUpInside)
    }
//
//    // MARK: Creation
//
//    static func button(icon:String!, xInset:CGFloat = 0, yInset:CGFloat = 0) -> UIButton {
//        return self.button(image: UIImage.init(named: icon), xInset: xInset, yInset: yInset)
//    }
//
//    static func button(icon:String!, sideInset:CGFloat = 0) -> UIButton {
//        return self.button(image: UIImage.init(named: icon), xInset: sideInset, yInset: sideInset)
//    }
//
//    static func button(image:UIImage!, sideInset:CGFloat = 0) -> UIButton {
//        return self.button(image: image, xInset: sideInset, yInset: sideInset)
//    }
//
//    //swiftlint:disable:next line_length
//    static func button(icon:String!, top:CGFloat = 0, left:CGFloat = 0, bottom:CGFloat = 0, right:CGFloat = 0, highlightedIcon:String? = nil) -> UIButton {
//        var highlightedImage : UIImage?
//        if let icon = highlightedIcon {
//            highlightedImage = UIImage(named: icon)
//        }
//        return button(image: UIImage.init(named: icon), top: top, left: left, bottom: bottom, right: right, highlightedImage:highlightedImage)
//    }
//
//    static func button(image:UIImage!, xInset:CGFloat = 0, yInset:CGFloat = 0) -> UIButton {
//        return button(image: image, top: yInset, left: xInset, bottom: yInset, right: xInset)
//    }
//
//    //swiftlint:disable:next line_length
//    static func button(image:UIImage!, top:CGFloat = 0, left:CGFloat = 0, bottom:CGFloat = 0, right:CGFloat = 0, highlightedImage:UIImage? = nil) -> UIButton {
//        let button = UIButton()
//
//        button.setImage(image.withRenderingMode(.alwaysOriginal), for: .normal)
//        if let highlighted = highlightedImage {
//            button.setImage(highlighted, for: .highlighted)
//        } else {
//            button.showsTouchWhenHighlighted = false
//        }
//
////        button.contentEdgeInsets = UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
//        button.frame.size.width = left + right + image.size.width
//        button.frame.size.height = bottom + top + image.size.height
//        return button
//    }
//
//    // MARK: Bounds
//
//    func setInsets(horizontal:CGFloat = 0, vertical:CGFloat = 0) {
//        var insets = self.contentEdgeInsets
//        insets.left = horizontal
//        insets.right = horizontal
//        insets.top = vertical
//        insets.bottom = vertical
//        self.contentEdgeInsets = insets
//    }
//
//    // MARK: Attributes
//
//    func setLetterSpacing(title:String!, value:CGFloat) {
//        self.setAttributedTitle(NSAttributedString(string: title, attributes: [.kern: value]), for: .normal)
//    }
}
//
//extension UIButton {
//    func centerTextAndImage(spacing: CGFloat) {
//        let insetAmount = spacing / 2
//        imageEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: -insetAmount)
//        titleEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount, bottom: 0, right: insetAmount)
//        contentEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount, bottom: 0, right: -insetAmount)
//    }
//}
