//
//  Dictionary+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 09.07.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import Foundation

public extension Dictionary {
	public func percentEscaped() -> String {
        return map { (key, value) in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }.joined(separator: "&")
    }
}
