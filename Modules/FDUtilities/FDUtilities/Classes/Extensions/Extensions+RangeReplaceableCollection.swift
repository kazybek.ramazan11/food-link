//
//  Extensions+RangeReplaceableCollection.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 30.10.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import Foundation

extension RangeReplaceableCollection where Indices: Equatable {
    mutating func rearrange(from: Index, to: Index) {
        guard from != to, indices.contains(from), indices.contains(to) else { return }
        insert(remove(at: from), at: to)
    }
}
