//
//  Encodable+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 19.08.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import Foundation

extension Encodable {
    var dictionary: [String: Any] {
        guard let data = try? JSONEncoder().encode(self) else { return [:] }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }?.compactMapValues({ $0 }) ?? [:]
    }
}
