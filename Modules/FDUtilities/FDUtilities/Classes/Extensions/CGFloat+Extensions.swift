//
//  CGFloat+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 04.08.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit

extension CGFloat {
    var degreesToRadians: CGFloat { return self * .pi / 180 }
    var radiansToDegrees: CGFloat { return self * 180 / .pi }
}
