//
//  UIDevice+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 09.07.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit

public extension UIDevice {
    static let deviceName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            guard let devicePath = Bundle.main.path(forResource: "iOSDeviceModel", ofType: "plist"),
                let deviceDictionary = NSDictionary(contentsOfFile: devicePath) as? [String: String] else {
                    return ""
            }
            
            return deviceDictionary[identifier] ?? ""
        }
        
        return mapToDevice(identifier: identifier)
    }()
    
    var hasNotch: Bool {
        return bottomSafeAreaHeight > 0
    }
    
    var bottomSafeAreaHeight: CGFloat {
        return UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
    }
}
