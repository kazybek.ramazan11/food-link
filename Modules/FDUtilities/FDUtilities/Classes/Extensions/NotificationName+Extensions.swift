//
//  NotificationName+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 24.08.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import Foundation

extension Notification.Name {
    public static let userDidSignin = NSNotification.Name("userDidSignIn")
    public static let userDidBindPhone = NSNotification.Name("userDidBindPhone")
    public static let partnerChanged = Notification.Name("partnerChanged")
    public static let partnertsSuccessFetch = Notification.Name("didPartnersSuccessFetch")
    public static let chatMessageDidMarkAsRead = Notification.Name("chatMessageDidMarkAsRead")
    public static let userDidBindCard = NSNotification.Name(rawValue: "userDidBindCard")
    public static let userDidSignIn = NSNotification.Name(rawValue: "userDidSignIn")
    public static let userDidBindEmail = NSNotification.Name(rawValue: "userDidBindEmail")
    public static let didFinishSendingPhoto = NSNotification.Name(rawValue: "didFinishSendingPhoto")
    public static let didSelectFilterType = NSNotification.Name(rawValue: "didSelectFilterType")
}
