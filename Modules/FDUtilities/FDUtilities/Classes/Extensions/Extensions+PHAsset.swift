//
//  Extensions+PHAsset.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 20.12.2020.
//  Copyright © 2020 chocolife.me. All rights reserved.
//

import Foundation
import Photos

extension PHAsset {
    public func getUIImage(withSize size: CGSize = CGSize(width: 200, height: 200)) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        
        option.isSynchronous = true
        manager.requestImage(for: self, targetSize: size, contentMode: .aspectFit, options: option, resultHandler: { (result, _) -> Void in
            thumbnail = result!
        })
        
        return thumbnail
    }
}
