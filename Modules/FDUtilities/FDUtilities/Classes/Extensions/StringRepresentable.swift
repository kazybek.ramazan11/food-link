//
//  StringRepresentable.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 18.08.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import Foundation

extension Int: StringRepresentable {}
extension Double: StringRepresentable {}

protocol StringRepresentable: CustomStringConvertible, Codable {
    init?(_ string: String)
}

struct StringType<Value: StringRepresentable>: Codable {
    var value: Value
    
    init(value: Value) {
        self.value = value
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        do {
            let string = try container.decode(String.self)
            
            guard let value = Value(string) else {
                throw DecodingError.dataCorruptedError(
                    in: container,
                    debugDescription: """
                    Failed to convert an instance of \(Value.self) from "\(string)"
                    """
                )
            }
            
            self.value = value
        } catch {
            let force = try container.decode(Value.self)
            self.value = force
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(value.description)
    }
}
