//
//  UIApplication+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 09.07.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit

extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
    
    // MARK: - Properties
    static let statusBarHeight: CGFloat = UIApplication.shared.statusBarFrame.height
    var window: UIWindow {
        guard let window = windows.first else { return UIWindow(frame: UIScreen.main.bounds) }
        
        return window
    }
    
    // MARK: - Methods
    func topViewController(controller: UIViewController? = UIApplication.shared.window.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        
        if let tabController = controller as? UITabBarController,
            let selectedViewController = tabController.selectedViewController {
            return topViewController(controller: selectedViewController)
        }
        
        if let presentedViewController = controller?.presentedViewController {
            return topViewController(controller: presentedViewController)
        }
        
        return controller
    }
    
    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {

        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)
        } else if let tab = base as? UITabBarController {//, let selected = tab.selectedViewController {
            return tab
        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        
        return base
    }
    
//    class func getTopNavigationController() -> UINavigationController? {
//        guard let topMainViewController = UIApplication.getTopViewController() as? MainViewController else { return nil }
//        guard let topNavigationController = topMainViewController.children.first as? UINavigationController else { return nil }
//        return topNavigationController
//    }
}
