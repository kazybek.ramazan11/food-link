//
//  UITableView+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 30.10.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit

public extension UITableView {
    func dequeueReusableCell<T: UITableViewCell>(withClass name: T.Type, for indexPath: IndexPath) -> T {
        guard let cell = self.dequeueReusableCell(withIdentifier: T.name, for: indexPath) as? T else {
            fatalError("\(T.self) not registered to Table")
        }
        return cell
    }
    
    func register<T: UITableViewCell>(_ aClass: T.Type) {
        self.register(aClass, forCellReuseIdentifier: T.name)
    }
    
    var totalNumberOfRows: Int {
        var totalNumber = 0
        for section in 0 ..< numberOfSections {
            for _ in 0 ..< numberOfRows(inSection: section) {
                totalNumber += 1
            }
        }
        
        return totalNumber
    }
    
//    func displayHideEmptyPlaceholder(with type: PlaceholderType) {
//        if totalNumberOfRows != 0 {
//            backgroundView = nil
//        } else {
//            let emptyPlaceholderView = EmptyPlaceholderView(type: type)
//            backgroundView = emptyPlaceholderView
//        }
//    }
    
    //Variable-height UITableView tableHeaderView with autolayout
    func layoutTableHeaderFooterView() {
        
        let views = [self.tableHeaderView, self.tableFooterView]
        
        for (index, view) in views.enumerated() {
            
            guard let view = view else { continue }
            
            view.translatesAutoresizingMaskIntoConstraints = false
            
            let viewWidth = view.bounds.size.width
            let temporaryWidthConstraints = NSLayoutConstraint.constraints(withVisualFormat: "[view(width)]", options: NSLayoutConstraint.FormatOptions(rawValue: UInt(0)), metrics: ["width": viewWidth], views: ["view": view])
            
            view.addConstraints(temporaryWidthConstraints)
            
            view.setNeedsLayout()
            view.layoutIfNeeded()
            
            let viewSize = view.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
            let height = viewSize.height
            var frame = view.frame
            
            frame.size.height = height
            view.frame = frame
            
            if index == 0 {
                self.tableHeaderView = view
            } else {
                self.tableFooterView = view
            }
            
            view.removeConstraints(temporaryWidthConstraints)
            view.translatesAutoresizingMaskIntoConstraints = true
            
            
        }
        
    }
    
    func scrollToBottom() {
        DispatchQueue.main.async {
            guard self.numberOfSections > 0 else { return }
            
            var section = max(self.numberOfSections - 1, 0)
            var row = max(self.numberOfRows(inSection: section) - 1, 0)
            var indexPath = IndexPath(row: row, section: section)
            
            while !self._indexPathIsValid(indexPath) {
                section = max(section - 1, 0)
                row = max(self.numberOfRows(inSection: section) - 1, 0)
                indexPath = IndexPath(row: row, section: section)
                
                if indexPath.section == 0 {
                    indexPath = IndexPath(row: 0, section: 0)
                    break
                }
            }
            
            guard self._indexPathIsValid(indexPath) else { return }
            
            self.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    func scrollToTop(_ animated: Bool = true) {
        Thread.onMainThread { [unowned self] in
            let numberOfSections = self.numberOfSections
            let numberOfRows = self.numberOfRows(inSection: numberOfSections-1)
            
            guard numberOfRows > 0 else { return }
            
            let indexPath = IndexPath(row: 0, section: 0)
            self.scrollToRow(at: indexPath, at: .top, animated: animated)
        }
    }
    
    private func _indexPathIsValid(_ indexPath: IndexPath) -> Bool {
        let section = indexPath.section
        let row = indexPath.row
        return section < numberOfSections && row < numberOfRows(inSection: section)
    }
    
    func reloadDataWithoutChangingOffset() {
        UIView.setAnimationsEnabled(false)
        beginUpdates()
        reloadSections(IndexSet(integer: 1), with: .none)
        endUpdates()
    }
}
