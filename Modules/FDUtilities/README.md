# FDUtilities

[![CI Status](https://img.shields.io/travis/iBol/FDUtilities.svg?style=flat)](https://travis-ci.org/iBol/FDUtilities)
[![Version](https://img.shields.io/cocoapods/v/FDUtilities.svg?style=flat)](https://cocoapods.org/pods/FDUtilities)
[![License](https://img.shields.io/cocoapods/l/FDUtilities.svg?style=flat)](https://cocoapods.org/pods/FDUtilities)
[![Platform](https://img.shields.io/cocoapods/p/FDUtilities.svg?style=flat)](https://cocoapods.org/pods/FDUtilities)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FDUtilities is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FDUtilities'
```

## Author

iBol, aibolseed@gmail.com

## License

FDUtilities is available under the MIT license. See the LICENSE file for more info.
