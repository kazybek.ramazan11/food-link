//
//  UserSession.swift
//  FDFoundation
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import EasyDi

public protocol UserSessionCredentials {
	var accessToken: String { get }
	var refreshToken: String { get }
}

public enum UserPinState: Equatable {
	case created(pin: String)
	case declined
	case notSet
}

public final class UserSessionKit: Assembly {
	private lazy var storageKit: UserSessionStorageKit = DIContext.defaultInstance.assembly()
	
	public var service: UserSession {
		return define(scope: Scope.lazySingleton, init: UserSession(storage: self.storageKit.storage))
	}
}

public final class UserSession {
	private let storage: UserSessionStorage

	public var pinState: UserPinState {
		get {
			switch storage.pin {
			case let .some(pin) where pin.isEmpty:
				return .declined
			case let .some(pin):
				return .created(pin: pin)
			case .none:
				return .notSet
			}
		}
		set {
			switch newValue {
			case let .created(pin):
				storage.pin = pin
			case .declined:
				storage.pin = ""
			case .notSet:
				storage.pin = nil
			}
		}
	}

	public var otpId: String? {
		get {
			storage.otpId
		}
		set {
			let valueToSet = newValue != nil ? newValue : storage.otpId
			storage.otpId = valueToSet
		}
	}

	public var isBiometricAuthBeingUsed: Bool {
		get {
			storage.isBiometricAuthBeingUsed
		}
		set {
			storage.isBiometricAuthBeingUsed = newValue
		}
	}

	public var isExists: Bool {
		storage.refreshToken != nil && storage.pin != nil
	}

	public init(storage: UserSessionStorage) {
		self.storage = storage
	}

	public func start(with credentials: UserSessionCredentials) {
		storage.accessToken = credentials.accessToken
		storage.refreshToken = credentials.refreshToken
	}

	public func finish() {
		storage.clearAll()
	}
}
