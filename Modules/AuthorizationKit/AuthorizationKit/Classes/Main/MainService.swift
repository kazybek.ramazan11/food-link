//
//  MainService.swift
//  AuthorizationKit
//
//  Created by iBol on 4/5/21.
//

import FDNetworkLayer
import UIKit

public protocol IMainService {
	func validatePhone(with endpoint: MainEndpoint, and completion: @escaping ResultClosure<ValidatePhoneResponseModel>)
	func register(with endpoint: MainEndpoint, and completion: @escaping ResultClosure<AuthModels.Register.Response>)
	func auth(with endpoint: MainEndpoint, and completion: @escaping ResultClosure<AuthModels.Auth.Response>)
	func requestCode(with endpoint: MainEndpoint, and completion: @escaping ResultClosure<AuthModels.SendOtp.Response>)
	func verifyOtp(with endpoint: MainEndpoint, and completion: @escaping ResultClosure<AuthModels.VerifyOtp.Response>)
}

public final class MainService: APIClient, IMainService {
	public var session: URLSession = {
		let config = URLSessionConfiguration.init()
		config.isDiscretionary = true
		
		let session = URLSession(configuration: .default)
		return session
	}()
	
	public func validatePhone(with endpoint: MainEndpoint, and completion: @escaping ResultClosure<ValidatePhoneResponseModel>) {
		fetch(with: endpoint.request, completion: completion)
	}
	
	public func register(with endpoint: MainEndpoint, and completion: @escaping ResultClosure<AuthModels.Register.Response>) {
		fetch(with: endpoint.request, completion: completion)
	}
	
	public func auth(with endpoint: MainEndpoint, and completion: @escaping ResultClosure<AuthModels.Auth.Response>) {
		fetch(with: endpoint.request, completion: completion)
	}
	
	public func requestCode(with endpoint: MainEndpoint, and completion: @escaping ResultClosure<AuthModels.SendOtp.Response>) {
		fetch(with: endpoint.request, completion: completion)
	}
	
	public func verifyOtp(with endpoint: MainEndpoint, and completion: @escaping ResultClosure<AuthModels.VerifyOtp.Response>) {
		fetch(with: endpoint.request, completion: completion)
	}
}
