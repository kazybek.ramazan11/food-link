//
//  AuthorizationKitAssembly.swift
//  AuthorizationKit
//
//  Created by iBol on 5/8/21.
//

import EasyDi
import Foundation
import FDNetworkLayer

public final class AuthorizationKitAssembly: Assembly {
	// MARK: - Params
	let networkAssembly: NetworkAssembly = DIContext.defaultInstance.assembly()
	
	// MARK: - History Service
	public var service: IMainService {
		let service = MainService()
		return define(scope: Scope.lazySingleton, init: service)
	}
}
