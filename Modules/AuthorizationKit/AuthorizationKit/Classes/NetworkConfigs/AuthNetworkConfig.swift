//
//  AuthNetworkConfig.swift
//  AuthorizationKit
//
//  Created by iBol on 5/8/21.
//

import FDNetworkLayer
import EasyDi

public struct AuthNetworkConfig: INetworkConfigs {
	
	public var baseURL: String {
		"https://foodlinkback.herokuapp.com/api/v1"
	}
	
	public var port: Int? {
		return nil
	}
	
	public init() {}
}
