# AuthorizationKit

[![CI Status](https://img.shields.io/travis/iBol/AuthorizationKit.svg?style=flat)](https://travis-ci.org/iBol/AuthorizationKit)
[![Version](https://img.shields.io/cocoapods/v/AuthorizationKit.svg?style=flat)](https://cocoapods.org/pods/AuthorizationKit)
[![License](https://img.shields.io/cocoapods/l/AuthorizationKit.svg?style=flat)](https://cocoapods.org/pods/AuthorizationKit)
[![Platform](https://img.shields.io/cocoapods/p/AuthorizationKit.svg?style=flat)](https://cocoapods.org/pods/AuthorizationKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

AuthorizationKit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'AuthorizationKit'
```

## Author

iBol, aibolseed@gmail.com

## License

AuthorizationKit is available under the MIT license. See the LICENSE file for more info.
