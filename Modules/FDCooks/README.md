# FDCooks

[![CI Status](https://img.shields.io/travis/iBol/FDCooks.svg?style=flat)](https://travis-ci.org/iBol/FDCooks)
[![Version](https://img.shields.io/cocoapods/v/FDCooks.svg?style=flat)](https://cocoapods.org/pods/FDCooks)
[![License](https://img.shields.io/cocoapods/l/FDCooks.svg?style=flat)](https://cocoapods.org/pods/FDCooks)
[![Platform](https://img.shields.io/cocoapods/p/FDCooks.svg?style=flat)](https://cocoapods.org/pods/FDCooks)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FDCooks is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FDCooks'
```

## Author

iBol, aibolseed@gmail.com

## License

FDCooks is available under the MIT license. See the LICENSE file for more info.
