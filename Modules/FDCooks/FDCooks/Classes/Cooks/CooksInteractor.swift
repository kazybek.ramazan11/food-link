//
//  CooksInteractor.swift
//  FDCooks
//
//  Created by iBol on 5/10/21.
//

import UIKit

protocol ICooksInteractor: AnyObject {}

final class CooksInteractor: ICooksInteractor {
	// MARK: - Deps
	
	weak var controller: ICooksView?
	weak var coordinator: ICooksCoordinatorOutput?
}
