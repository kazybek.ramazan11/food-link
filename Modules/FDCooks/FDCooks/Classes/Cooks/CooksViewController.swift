//
//  CooksViewController.swift
//  FDCooks
//
//  Created by iBol on 5/10/21.
//

import UIKit
import SnapKit

protocol ICooksView: AnyObject {}

final class CooksViewController: UIViewController {
	// MARK: - Deps
	
	private let interactor: ICooksInteractor
	
	// MARK: - Lifecycle
	
	init(interactor: ICooksInteractor) {
		self.interactor = interactor
		
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		view.backgroundColor = .red
	}
}

// MARK: - ICooksView

extension CooksViewController: ICooksView {}
