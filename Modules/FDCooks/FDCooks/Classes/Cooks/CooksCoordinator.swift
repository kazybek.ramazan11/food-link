//
//  CooksCoordinator.swift
//  FDCooks
//
//  Created by iBol on 5/10/21.
//

import FDUIKit
import EasyDi

public typealias ICooksCoordinatorOutput = (ICooksCoordinator & ExitCoordinator)

public protocol ICooksCoordinator: AnyObject {}

public final class CooksCoordinator: BaseCoordinator, ICooksCoordinatorOutput {
	// MARK: - ExitCoordinator
	
	public var exitCompletion: Callback?
	
	// MARK: - Lifecycle
	
	public override init(router: Router) {
		super.init(router: router)
	}
	
	public override func start() {
		navigateToCooks()
	}
	
	private func navigateToCooks() {
		let assembly: CooksAssembly = DIContext.defaultInstance.assembly()
		let controller = assembly.makeModule(with: self)
		router.push(controller, animated: true, hideBottomBarWhenPushed: false, completion: nil)
	}
}
