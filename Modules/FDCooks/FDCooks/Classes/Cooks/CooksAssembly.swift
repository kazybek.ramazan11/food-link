//
//  CooksAssembly.swift
//  FDCooks
//
//  Created by iBol on 5/10/21.
//

import EasyDi

final class CooksAssembly: Assembly {
	// MARK: - ViewController
	
	func makeModule(with coordinator: ICooksCoordinatorOutput) -> UIViewController {
		let interactor = makeInteractor()
		let controller = CooksViewController(interactor: interactor)
		
		return define(init: controller) {
			interactor.controller = $0
			interactor.coordinator = coordinator
			return $0
		}
	}
	
	// MARK: - Interactor
	
	private func makeInteractor() -> CooksInteractor {
		return CooksInteractor()
	}
}
