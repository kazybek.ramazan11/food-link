//
//  OrdersInteractor.swift
//  FDOrders
//
//  Created by iBol on 5/10/21.
//

import UIKit

protocol IOrdersInteractor: AnyObject {}

final class OrdersInteractor: IOrdersInteractor {
	// MARK: - Deps
	
	weak var controller: IOrdersView?
	weak var coordinator: IOrdersCoordinatorOutput?
}
