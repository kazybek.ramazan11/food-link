//
//  OrdersAssembly.swift
//  FDOrders
//
//  Created by iBol on 5/10/21.
//

import EasyDi

final class OrdersAssembly: Assembly {
	// MARK: - ViewController
	
	func makeModule(with coordinator: IOrdersCoordinatorOutput) -> UIViewController {
		let interactor = makeInteractor()
		let controller = OrdersViewController(interactor: interactor)
		
		return define(init: controller) {
			interactor.controller = $0
			interactor.coordinator = coordinator
			return $0
		}
	}
	
	// MARK: - Interactor
	
	private func makeInteractor() -> OrdersInteractor {
		return OrdersInteractor()
	}
}
