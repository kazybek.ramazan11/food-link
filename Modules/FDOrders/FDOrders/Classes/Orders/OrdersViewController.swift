//
//  OrdersViewController.swift
//  FDOrders
//
//  Created by iBol on 5/10/21.
//

import UIKit
import SnapKit

protocol IOrdersView: AnyObject {}

final class OrdersViewController: UIViewController {
	// MARK: - Deps
	
	private let interactor: IOrdersInteractor
	
	// MARK: - Lifecycle
	
	init(interactor: IOrdersInteractor) {
		self.interactor = interactor
		
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		view.backgroundColor = .green
	}
}

// MARK: - IOrdersView

extension OrdersViewController: IOrdersView {}
