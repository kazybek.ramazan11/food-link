//
//  OrdersCoordinator.swift
//  FDOrders
//
//  Created by iBol on 5/10/21.
//

import FDUIKit
import EasyDi

public typealias IOrdersCoordinatorOutput = (IOrdersCoordinator & ExitCoordinator)

public protocol IOrdersCoordinator: AnyObject {}

public final class OrdersCoordinator: BaseCoordinator, IOrdersCoordinatorOutput {
	// MARK: - ExitCoordinator
	
	public var exitCompletion: Callback?
	
	// MARK: - Lifecycle
	
	public override init(router: Router) {
		super.init(router: router)
	}
	
	public override func start() {
		navigateToOrders()
	}
	
	private func navigateToOrders() {
		let assembly: OrdersAssembly = DIContext.defaultInstance.assembly()
		let controller = assembly.makeModule(with: self)
		router.push(controller, hideBottomBarWhenPushed: false, completion: nil)
	}
}
