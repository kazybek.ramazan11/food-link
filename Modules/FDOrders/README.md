# FDOrders

[![CI Status](https://img.shields.io/travis/iBol/FDOrders.svg?style=flat)](https://travis-ci.org/iBol/FDOrders)
[![Version](https://img.shields.io/cocoapods/v/FDOrders.svg?style=flat)](https://cocoapods.org/pods/FDOrders)
[![License](https://img.shields.io/cocoapods/l/FDOrders.svg?style=flat)](https://cocoapods.org/pods/FDOrders)
[![Platform](https://img.shields.io/cocoapods/p/FDOrders.svg?style=flat)](https://cocoapods.org/pods/FDOrders)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FDOrders is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FDOrders'
```

## Author

iBol, aibolseed@gmail.com

## License

FDOrders is available under the MIT license. See the LICENSE file for more info.
