//
//  RegistrationView.swift
//  Authorization
//
//  Created by iBol on 5/3/21.
//

import FDUIKit

protocol RegistrationViewDelegate: AnyObject {
	func didTapRegister(with password: String)
	func didBackButtonTapped()
}

class RegistrationView: UIView {
	// MARK: - Deps
	public weak var delegate: RegistrationViewDelegate?
	// MARK: - Variables
	private let phoneNumber: String
	
	// MARK: - Views Factory
	private let logoImageView: UIImageView =
		UIImageViewFactory(image: UIImage.makeFromBundle(imageName: "logo-with-text"))
		.build()
	
	private let titleLabel: UILabel =
		UILabelFactory(text: "Регистрация")
		.font(FontFamily.Roboto.medium.font(size: 20))
		.text(color: .appBlack)
		.text(alignment: .center)
		.build()
	
	private lazy var phoneNumberTextField: TextFieldWithPadding = {
		let padding: UIEdgeInsets = .init(top: 7, left: 15, bottom: 7, right: 15)
		let shadowOffset = CGSize(width: 0, height: 4)
		let view = TextFieldWithPadding(padding: padding)
		view.backgroundColor = .fieldBackgroundGray
		view.corner(radius: 7)
		view.rightView = phoneNumberRightView
		view.rightViewMode = .always
		view.isUserInteractionEnabled = false
		view.attributedPlaceholder = NSAttributedString(string: phoneNumber, attributes: [NSAttributedString.Key.foregroundColor: UIColor.placeholderGray])
		view.font = FontFamily.Roboto.regular.font(size: 14)
		view.addShadow(ofColor: .white, radius: 10, offset: shadowOffset, opacity: 0.1)
		return view
	}()
	
	private var phoneNumberRightView = UIView()
	private var rightImageView: UIImageView = {
		let image = UIImage.makeFromBundle(imageName: "correct-tick")
		let imageView = UIImageView(image: image)
		return imageView
	}()
	
	private let passwordTextField: TextFieldWithPadding = {
		let padding: UIEdgeInsets = .init(top: 7, left: 15, bottom: 7, right: 15)
		let view = TextFieldWithPadding(padding: padding)
		let shadowOffset = CGSize(width: 0, height: 4)
		view.corner(radius: 7)
		view.placeholder = "Придумайте пароль"
		view.font = FontFamily.Roboto.regular.font(size: 14)
		view.rightView = UIImageView(image: UIImage.makeFromBundle(imageName: "eyes-open"))
		view.addShadow(ofColor: .white, radius: 10, offset: shadowOffset, opacity: 0.1)
		view.backgroundColor = .fieldBackgroundGray
		view.isSecureTextEntry = true
		return view
	}()
	
	private let continueButton: UIButton = {
		let view = UIButton.primaryButton(title: "Зарегистрироваться")
		let shadowOffset = CGSize(width: 0, height: 4)
		view.addShadow(ofColor: .white, radius: 10, offset: shadowOffset, opacity: 0.1)
		view.tap(target: self, selector: #selector(didTapContinueButton))
		return view
	}()
	
	private lazy var backButton: UIButton =
		UIButtonFactory(image: backImage)
		.tint(color: .appBlack)
		.addTarget(self, action: #selector(didBackButtonTapped))
		.build()
	
	private lazy var backImage: UIImage = {
		guard let image = UIImage.makeFromBundle(imageName: "left-arrow")?.withRenderingMode(.alwaysTemplate) else {
			return UIImage()
		}
		return image
	}()
	
	init(delegate: RegistrationViewDelegate, phoneNumber: String) {
		self.delegate = delegate
		self.phoneNumber = phoneNumber
		super.init(frame: .zero)
		
		backgroundColor = .white
		addSubviews()
		setupLayout()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	// MARK: - Actions
	@objc private func didTapContinueButton() {
		guard let password = passwordTextField.text else { return }
		delegate?.didTapRegister(with: password)
	}
	
	@objc private func didBackButtonTapped() {
		delegate?.didBackButtonTapped()
	}
}

extension RegistrationView: Decoratable {
	public func addSubviews() {
		[logoImageView, titleLabel, phoneNumberTextField, continueButton, passwordTextField, backButton].forEach { addSubview($0) }
		phoneNumberRightView.addSubview(rightImageView)
	}
	
	public func setupLayout() {
		backButton.snp.makeConstraints { make in
			make.centerY.equalTo(logoImageView)
			make.left.equalToSuperview().offset(20)
			make.width.height.equalTo(30)
		}
		
		logoImageView.snp.makeConstraints { make in
			make.top.equalTo(safeAreaLayoutGuide).offset(22)
			make.centerX.equalToSuperview()
		}
		
		titleLabel.snp.makeConstraints { make in
			make.centerX.equalToSuperview()
		}
		
		phoneNumberTextField.snp.makeConstraints { make in
			make.top.equalTo(titleLabel.snp.bottom).offset(20)
			make.left.equalToSuperview().offset(35)
			make.centerX.equalToSuperview()
			make.height.equalTo(48)
		}
		
		passwordTextField.snp.makeConstraints { make in
			make.top.equalTo(phoneNumberTextField.snp.bottom).offset(22)
			make.left.right.height.equalTo(phoneNumberTextField)
			make.centerY.equalToSuperview()
		}
		
		continueButton.snp.makeConstraints { make in
			make.bottom.equalTo(safeAreaLayoutGuide).inset(50)
			make.left.right.equalTo(passwordTextField)
			make.height.equalTo(48)
		}
		
		rightImageView.snp.makeConstraints { make in
			make.width.height.equalTo(20)
			make.right.equalToSuperview().offset(-15)
			make.left.equalToSuperview().offset(15)
			make.top.bottom.equalToSuperview()
		}
	}
}

