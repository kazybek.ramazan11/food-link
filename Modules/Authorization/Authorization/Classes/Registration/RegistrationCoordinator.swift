//
//  RegistrationCoordinator.swift
//  Authorization
//
//  Created by iBol on 5/3/21.
//

import FDUIKit
import EasyDi

public typealias IRegistrationCoordinatorOutput = (IRegistrationCoordinator & IRegistrationCoordinatorFlow & BaseCoordinator)

public protocol IRegistrationCoordinatorFlow: AnyObject {}

public protocol IRegistrationCoordinator: AnyObject {
	func showOtp(with phoneNumber: String)
	func presentController(with controller: Presentable)
}

public final class RegistrationCoordinator: IRegistrationCoordinatorOutput, ExitCoordinator {
	// MARK: - Dependencies
	public var exitCompletion: Callback?
	private var phoneNumber: String
	
	// MARK: Init
	public init(router: Router, phoneNumber: String) {
		self.phoneNumber = phoneNumber
		super.init(router: router)
	}
	
	override public func start() {
		showRegistration()
	}
	
	public func showRegistration() {
		let registrationAssembly: RegistrationAssembly = DIContext.defaultInstance.assembly()
		var registrationModule = registrationAssembly.makeRegistration(coordinator: self, phoneNumber: phoneNumber)
		router.push(registrationModule)
	}
	
	public func showOtp(with phoneNumber: String) {
		let coordinator = OtpCoordinator(router: router, phoneNumber: phoneNumber)
		coordinator.start()
	}
	
	public func presentController(with controller: Presentable) {
		router.presentBottomSheet(controller)
	}
}
