//
//  ProfileAssembly.swift
//  food_link
//
//  Created by Ramazan Kazybek on 5/7/21.
//

import EasyDi
import FDFoundation
import AuthorizationKit

final class RegistrationAssembly: Assembly {
	private let authorizationKitAssembly: AuthorizationKitAssembly = DIContext.defaultInstance.assembly()
	
	func makeRegistration(coordinator: RegistrationCoordinator, phoneNumber: String) -> RegistrationPresentable {
		let interactor = RegistrationInteractor(coordinator: coordinator, service: authorizationKitAssembly.service, phoneNumber: phoneNumber)
		let controller = RegistrationViewController(interactor: interactor)
		return define(init: controller) {
			interactor.controller = controller
			return $0
		}
	}
}

