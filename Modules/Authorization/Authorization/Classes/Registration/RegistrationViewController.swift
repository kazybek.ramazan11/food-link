//
//  RegistrationViewController.swift
//  Authorization
//
//  Created by iBol on 5/3/21.
//

import FDUIKit
import UIKit

protocol RegistrationPresentable: Presentable {}
protocol IRegistrationView: AnyObject {}

public class RegistrationViewController: UIViewController, RegistrationPresentable {
	// MARK: Deps
	private let interactor: RegistrationInteractor
	
	// MARK: Init
	init(interactor: RegistrationInteractor) {
		self.interactor = interactor
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	// MARK: - Lifecycle
	public override func loadView() {
		view = RegistrationView(delegate: self, phoneNumber: interactor.getPhoneNumber())
	}
	
	public override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		navigationController?.isNavigationBarHidden = true
	}
	
	public override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		navigationController?.isNavigationBarHidden = false
	}
}

// MARK: - RegistrationViewDelegate
extension RegistrationViewController: RegistrationViewDelegate {
	func didTapRegister(with password: String) {
		interactor.register(with: password)
//		interactor.showOtp()
	}
	
	func didBackButtonTapped() {
		navigationController?.popViewController(animated: true)
	}
}
