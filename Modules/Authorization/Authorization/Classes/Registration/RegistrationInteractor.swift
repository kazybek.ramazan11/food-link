//
//  RegistrationInteractor.swift
//  Authorization
//
//  Created by iBol on 5/3/21.
//

import UIKit
import AuthorizationKit
import FDNetworkLayer

protocol IRegistrationInteractor {
	func showOtp()
	func getPhoneNumber() -> String
}

final class RegistrationInteractor {
	// MARK: - Dependencies
	weak var controller: RegistrationViewController?
	var coordinator: IRegistrationCoordinatorOutput?
	private let service: IMainService
	private var phoneNumber: String
	
	public init(controller: RegistrationViewController? = nil,
				coordinator: IRegistrationCoordinatorOutput? = nil,
				service: IMainService,
				phoneNumber: String) {
		self.controller = controller
		self.coordinator = coordinator
		self.service = service
		self.phoneNumber = phoneNumber
	}
	
	public func getPhoneNumber() -> String {
		return phoneNumber
	}
	
	func register(with password: String) {
		let model = AuthModels.Register.Request(phoneNumber: phoneNumber, password: password)
		service.register(with: .register(model)) { [weak self] result in
			DispatchQueue.main.async {
				switch result {
				case .success:
					self?.showOtp()
				case let .failure(error):
					print(error.localizedDescription)
				}
			}
		}
	}
	
	func showOtp() {
		coordinator?.showOtp(with: phoneNumber)
	}
}
