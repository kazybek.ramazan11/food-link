//
//  PinCreateAssembly.swift
//  food_link
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import EasyDi
import FDFoundation

final class PinCreateAssembly: Assembly {
	private lazy var userSessionKit: UserSessionKit = DIContext.defaultInstance.assembly()
	private lazy var biometricAuthServiceKit: BiometricAuthServiceKit = DIContext.defaultInstance.assembly()
	
	func makePinCreate(coordinator: PinCreateCoordinator) -> PinCreatePresentable {
		let interactor = PinCreateInteractor(userSession: userSessionKit.service, biometricAuthService: biometricAuthServiceKit.service)
		let controller = PinCreateViewController(interactor: interactor)
		return define(init: controller) {
			interactor.controller = controller
			interactor.coordinator = coordinator
			return $0
		}
	}
}
