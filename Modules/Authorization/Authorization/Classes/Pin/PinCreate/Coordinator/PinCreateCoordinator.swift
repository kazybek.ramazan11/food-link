//
//  PinCreateCoordinator.swift
//  food_link
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import EasyDi
import FDFoundation
import FDUIKit

public protocol PinCreateCoordinatorOutput: class {
	var onFlowDidFinish: ((_ isSwipedBack: Bool) -> Void)? { get set }
	var onPinCreated: Callback? { get set }
}

public final class PinCreateCoordinator: Coordinator, PinCreateCoordinatorOutput, ExitCoordinator {
	public var onFlowDidFinish: ((_ isSwipedBack: Bool) -> Void)?
	public var onPinCreated: Callback?
	public var exitCompletion: Callback?
	
	private let router: Router
	
	public init(router: Router) {
		self.router = router
	}

	public func start() {
		showPinCreate()
	}

	private func showPinCreate() {
		let pinCreateAssembly: PinCreateAssembly = DIContext.defaultInstance.assembly()
		var pinCreate = pinCreateAssembly.makePinCreate(coordinator: self)
		pinCreate.onBackButtonDidTap = { [weak self] in
			self?.onFlowDidFinish?(false)
		}
		pinCreate.onBackDidSwipe = { [weak self] in
			self?.onFlowDidFinish?(true)
		}
		
		router.push(pinCreate)
	}
	
	func pinCreateFinished(isCreated: Bool) {
		guard isCreated else { return }
		onPinCreated?()
	}
	
	func showBiometricAuthUsage() {
		let title = "Вход по отпечатку пальца или скану лица"
		let description = "Включите, чтобы каждый раз не вводить пин-код."
		let turnOn = "Включить"
		let cancel = "Закрыть"
		let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)
		let turnOnAction = UIAlertAction(title: turnOn, style: .default, handler: turnOnCompletion())
		alert.addAction(turnOnAction)
		let cancelAction = UIAlertAction(title: cancel, style: .cancel)
		alert.addAction(cancelAction)
		router.present(alert)
	}
	
	func turnOnCompletion() -> ((UIAlertAction) -> Void)? { { _ in
//		guard let self = self else { return }
	}
	}
}

