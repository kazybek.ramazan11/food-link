//
//  PinCreateInteractor.swift
//  food_link
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import FDFoundation

@objc protocol IPinInteractor: AnyObject {
	func refreshPinViewModel()
	func didFillAllDots()
	func didTapDigitButton(digit: String)
	func didTapDeleteButton()
	@objc optional func didTapBiometricAuthButton()
}

final class PinCreateInteractor: IPinInteractor {
	private let userSession: UserSession
	weak var controller: IPinViewController?
	weak var coordinator: PinCreateCoordinator?
	private let pinCreateFacade = PinCreateFacade()
	private let biometricAuthService: BiometricAuthService
	
	private var viewModel: PinViewModel {
		PinViewModel(title: pinCreateFacade.currentStep.title, activeDotsCount: pinCreateFacade.currentStep.pin.length)
	}
	
	init(userSession: UserSession, biometricAuthService: BiometricAuthService) {
		self.userSession = userSession
		self.biometricAuthService = biometricAuthService
	}
	
	func refreshPinViewModel() {
		controller?.enter(viewModel: viewModel)
	}
	
	func didFillAllDots() {
		pinCreateFacade.changeStepIfNeeded()
		validatePin()
	}
	
	func didTapDigitButton(digit: String) {
		pinCreateFacade.push(digit)
		refreshPinViewModel()
	}
	
	func didTapDeleteButton() {
		pinCreateFacade.pop()
		refreshPinViewModel()
	}
	
	private func validatePin() {
		if pinCreateFacade.isFinished {
			switch pinCreateFacade.validate() {
			case let .valid(pin):
				userSession.pinState = .created(pin: pin)
				coordinator?.pinCreateFinished(isCreated: true)
//					state = .finished(isCreated: true, completion: { [weak self] in
//						guard let self = self  else { return }
//						self.pushManager.requestNotificationAuth()
//					})
			case let .invalid(errorMessage):
				controller?.error(message: errorMessage)
				pinCreateFacade.reset()
				controller?.enter(viewModel: viewModel)
			}
		} else {
			refreshPinViewModel()
		}
	}
	
	func didTapBiometricAuthButton() {
	}
}
