//
//  PinCreateFacade.swift
//  food_link
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import Foundation

final class PinCreateFacade {
	enum ValidationResult {
		case valid(pin: String)
		case invalid(errorMessage: String)
	}

	lazy var currentStep: PinCreateStep = initialStep

	private let initialStep = (title: "R.string.pinCreate.initialStepTitle()", pin: PinModel())
	private let repeatStep = (title: "R.string.pinCreate.repeatStepTitle()", pin: PinModel())

	var isFinished: Bool {
		initialStep.pin.isFilled && repeatStep.pin.isFilled
	}

	func changeStepIfNeeded() {
		currentStep = initialStep.pin.isFilled ? repeatStep : initialStep
	}

	func push(_ digit: String) {
		guard !isFinished else { return }
		currentStep.pin.addDigit(digit)
	}

	@discardableResult
	func pop() -> String? {
		guard !isFinished else { return nil }
		return currentStep.pin.removeLastDigit()
	}

	func reset() {
		initialStep.pin.reset()
		repeatStep.pin.reset()
		changeStepIfNeeded()
	}

	func validate() -> ValidationResult {
		if initialStep.pin.value == repeatStep.pin.value {
			return .valid(pin: initialStep.pin.value)
		} else {
			return .invalid(errorMessage: "R.string.pinCreate.pinMismatch()")
		}
	}
}
