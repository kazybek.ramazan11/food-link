//
//  PinCreateViewController.swift
//  food_link
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import FDUIKit

protocol PinCreatePresentable: Presentable {
	var onBackButtonDidTap: Callback? { get set }
	var onBackDidSwipe: Callback? { get set }
	var onCloseDidTap: Callback? { get set }
}

protocol IPinViewController: UIViewController {
	func enter(viewModel: PinViewModel)
	func error(message: String)
	func pinIncorrect()
}

final class PinCreateViewController: PinViewController, IPinViewController, PinCreatePresentable {
	var onCloseDidTap: Callback?
	var onBackButtonDidTap: Callback?
	var onBackDidSwipe: Callback?

	private let interactor: IPinInteractor

	init(interactor: IPinInteractor) {
		self.interactor = interactor
		super.init()
	}

	required init?(coder: NSCoder) {
		nil
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		interactor.refreshPinViewModel()
	}
	
	func enter(viewModel: PinViewModel) {
		navigationItem.title = viewModel.title
		configure(with: viewModel)
	}
	
	func error(message: String) {
		showSnackbar(type: .error(title: message))
		pinIncorrect()
	}
	
	func pinIncorrect() {
		shakeAndVibrate()
	}
	
//	override func customBackButtonDidTap() {
//		onBackButtonDidTap?()
//	}
//
//	override func transitionBackDidFinish() {
//		onBackDidSwipe?()
//	}

//	private func setupObservers() {
//		store.$state.observe(self) { vc, state in
//			guard let state = state else { return }
//			switch state {
//			case let .enter(viewModel):
//				vc.navigationItem.title = viewModel.title
//				vc.configure(with: viewModel)
//			case let .error(message):
//				vc.showSnackbar(type: .error(title: message))
//				vc.shakeAndVibrate()
//			case let .finished(isCreated, callback):
//				vc.onPinCreateDidFinish?(isCreated, callback)
//			}
//		}
//	}
}

extension PinCreateViewController {
	override func pinView(_ pinView: PinView, didFillDotsStackView dotsStackView: PinDotsStackView) {
		interactor.didFillAllDots()
	}

	override func pinView(_ pinView: PinView, didTapPinButton pinButton: PinButton) {
		switch pinButton.type {
		case .digit:
			interactor.didTapDigitButton(digit: pinButton.currentTitle ?? "")
		case .delete:
			interactor.didTapDeleteButton()
		case .biometricAuth:
			interactor.didTapBiometricAuthButton?()
		}
	}
}
