//
//  PinDotsStackView.swift
//  food_link
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import FDFoundation
import UIKit

private enum Constants {
	static let animationDuration: TimeInterval = 0.2
}

protocol PinDotsStackViewDelegate: class {
	func pinDotsStackView(_ pinDotsStackView: PinDotsStackView, didActivateDots activeDotsCount: Int)
}

final class PinDotsStackView: UIStackView {
	weak var delegate: PinDotsStackViewDelegate?

	var activeDotsCount = 0 {
		didSet {
			updateDotViews(oldValue)
		}
	}

	private lazy var dotViews: [UIView] = [0, 0, 0, 0].map { _ in 
		let view = UIView()
		view.layer.cornerRadius = 6
		return view
	}

	override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
	}

	required init(coder: NSCoder) {
		super.init(coder: coder)
		setup()
	}

	private func updateDotViews(_ oldActiveDotsCount: Int) {
		if oldActiveDotsCount < activeDotsCount {
			activateDotView(at: activeDotsCount - 1)
		} else {
			let dotViewIndexes = Array((activeDotsCount ..< oldActiveDotsCount).map { $0 }.reversed())
			deactivateDotViews(at: dotViewIndexes)
		}
	}

	private func activateDotView(at index: Int) {
		UIView.animate(withDuration: Constants.animationDuration,
					   animations: {
						self.dotViews[safe: index]?.backgroundColor = .green
						self.dotViews[safe: index]?.backgroundColor = UIColor.appBlack
					   },
					   completion: { _ in
						   self.delegate?.pinDotsStackView(self, didActivateDots: self.activeDotsCount)
		})
	}

	private func deactivateDotViews(at indexes: [Int]) {
		UIView.animate(withDuration: Constants.animationDuration) {
			indexes.forEach { self.dotViews[safe: $0]?.backgroundColor = .gray }
			indexes.forEach { self.dotViews[safe: $0]?.backgroundColor = UIColor.appOrange }
		}
	}

	private func setup() {
		distribution = .fillEqually
		spacing = 16
		dotViews.forEach { view in
			addArrangedSubview(view)
			view.addConstraint(NSLayoutConstraint(item: view,
												  attribute: .width,
												  relatedBy: .equal,
												  toItem: nil,
												  attribute: .notAnAttribute,
												  multiplier: 1,
												  constant: 12))
			view.addConstraint(NSLayoutConstraint(item: view,
												  attribute: .height,
												  relatedBy: .equal,
												  toItem: nil,
												  attribute: .notAnAttribute,
												  multiplier: 1,
												  constant: 12))
		}
	}
}
