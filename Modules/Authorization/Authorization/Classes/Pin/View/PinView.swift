//
//  PinView.swift
//  food_link
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import FDFoundation
import FDUIKit
import UIKit

protocol PinViewDelegate: class {
	func pinView(_ pinView: PinView, didTapPinButton pinButton: PinButton)
	func pinView(_ pinView: PinView, didFillDotsStackView dotsStackView: PinDotsStackView)
}

private enum Constants {
	static let animationKey = "position"
	static let animationDuration = 0.07
	static let animationRepeatCount: Float = 4
}

final class PinView: UIView, NibOwnerLoadable {
	var biometricType: BiometricAuthType = .none {
		didSet {
			biometricAuthButton.configure(with: biometricType)
		}
	}

	weak var delegate: PinViewDelegate?

	@IBOutlet private var dotsContainerViewHeightConstraint: NSLayoutConstraint!
	@IBOutlet private var dotsStackView: PinDotsStackView!
	@IBOutlet private var buttonsStackViewLeadingConstraint: NSLayoutConstraint!
	@IBOutlet private var buttonsStackViewTrailingConstraint: NSLayoutConstraint!
	@IBOutlet private var biometricAuthButton: PinButton!

	override init(frame: CGRect) {
		super.init(frame: frame)
		loadNibContent()
		dotsStackView.delegate = self
	}

	required init?(coder: NSCoder) {
		super.init(coder: coder)
		loadNibContent()
	}

	@IBAction private func didTapPinButton(_ sender: PinButton) {
		delegate?.pinView(self, didTapPinButton: sender)
	}

	override func awakeFromNib() {
		super.awakeFromNib()
		setupUIAdaptation()
	}

	func configure(with viewModel: PinViewModel) {
		dotsStackView.activeDotsCount = viewModel.activeDotsCount
	}

	func shakeAndVibrate() {
		FeedbackEmitter.error()

		let animation = CABasicAnimation(keyPath: Constants.animationKey)
		animation.duration = Constants.animationDuration
		animation.repeatCount = Constants.animationRepeatCount
		animation.autoreverses = true
		animation.fromValue = NSValue(cgPoint: CGPoint(x: dotsStackView.center.x - 8, y: dotsStackView.center.y))
		animation.toValue = NSValue(cgPoint: CGPoint(x: dotsStackView.center.x + 8, y: dotsStackView.center.y))

		dotsStackView.layer.add(animation, forKey: Constants.animationKey)
	}

	private func setupUIAdaptation() {
		guard Device.current.isSmall else { return }
		dotsContainerViewHeightConstraint.constant = 80
		buttonsStackViewLeadingConstraint.constant = 40
		buttonsStackViewTrailingConstraint.constant = 40
	}
}

extension PinView: PinDotsStackViewDelegate {
	func pinDotsStackView(_ pinDotsStackView: PinDotsStackView, didActivateDots activeDotsCount: Int) {
		guard activeDotsCount == 4 else { return }
		delegate?.pinView(self, didFillDotsStackView: dotsStackView)
	}
}
