//
//  PinButton.swift
//  food_link
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import Foundation
import FDFoundation
import FDUIKit
import UIKit

enum PinButtonType: Int {
	case digit, delete, biometricAuth
}

private enum Constants {
	static let defaultPinButtonType: PinButtonType = .digit
	static let animationDuration = 0.2
}

@IBDesignable
final class PinButton: UIButton {
	var type: PinButtonType {
		PinButtonType(rawValue: _type) ?? Constants.defaultPinButtonType
	}

	override var isHighlighted: Bool {
		didSet {
			updateHighlightedState()
		}
	}

	@IBInspectable private var _type: Int = Constants.defaultPinButtonType.rawValue

	override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}

	override func layoutSubviews() {
		super.layoutSubviews()
		layer.cornerRadius = frame.height / 2
	}

	func configure(with biometricAuthType: BiometricAuthType) {
		switch biometricAuthType {
		case .touchId:
//			configureBiometricImage(R.image.fingerPrintS())
			setVisibility(true)
		case .faceId:
//			configureBiometricImage(R.image.faceIDS())
			setVisibility(true)
		case .none:
			setVisibility(false)
		}
	}

	private func setVisibility(_ isVisible: Bool) {
		isEnabled = isVisible
		alpha = isEnabled ? 1 : 0
	}

	private func configureBiometricImage(_ image: UIImage?) {
		setImage(image, for: .normal)
		setImage(image, for: .highlighted)
		contentVerticalAlignment = .fill
		contentHorizontalAlignment = .fill
		imageEdgeInsets = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
	}

	// swiftlint:disable trailing_closure
	private func updateHighlightedState() {
		UIView.animate(withDuration: Constants.animationDuration,
					   delay: 0,
					   options: .allowUserInteraction,
					   animations: {
						self.backgroundColor = self.isHighlighted ? UIColor.black.withAlphaComponent(0.7) : UIColor.appOrange
		})
	}

	private func setup() {
		backgroundColor = UIColor.appOrange
		setTitleColor(.white, for: .normal)
		setTitleColor(UIColor.black.withAlphaComponent(0.3), for: .highlighted)
		titleLabel?.font = FontFamily.Roboto.medium.font(size: 36)
	}
}
