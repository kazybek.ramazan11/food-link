//
//  PinChangeInteractor.swift
//  food_link
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import FDFoundation

final class PinChangeInteractor: IPinInteractor {
	private var pinChangeFacade: PinChangeFacade?
	private let userSession: UserSession
	weak var controller: IPinViewController?
	weak var coordinator: PinChangeCoordinator?
	
	private var viewModel: PinViewModel {
		PinViewModel(title: pinChangeFacade?.currentStep.title ?? "", activeDotsCount: pinChangeFacade?.currentStep.pin.length ?? 4)
	}
	
	init(userSession: UserSession) {
		self.userSession = userSession
		if case let .created(pin) = userSession.pinState {
			self.pinChangeFacade = PinChangeFacade(authPinValue: pin)
		}
	}
	
	func refreshPinViewModel() {
		controller?.enter(viewModel: viewModel)
	}
	
	func didFillAllDots() {
		validatePin()
	}
	
	func didTapDigitButton(digit: String) {
		pinChangeFacade?.push(digit)
		verifyPinIfNeeded()
		refreshPinViewModel()
	}
	
	func didTapDeleteButton() {
		pinChangeFacade?.pop()
		refreshPinViewModel()
	}
	
	private func verifyPinIfNeeded() {
		guard let pinChangeFacade = pinChangeFacade,
			pinChangeFacade.isPinForVerificiationFinished else { return }
		switch pinChangeFacade.verify() {
		case .valid:
			break
		case let .invalid(message):
			pinChangeFacade.reset()
			controller?.pinIncorrect()
			controller?.error(message: message)
			refreshPinViewModel()
		case let .rejected(message):
			pinChangeFacade.reset()
			controller?.error(message: message)
		}
	}

	private func validatePin() {
		guard let pinChangeFacade = pinChangeFacade else { return }
		if pinChangeFacade.isPinForVerificiationFinished, case .valid = pinChangeFacade.verify() {
			pinChangeFacade.changeStepIfNeeded()
			if pinChangeFacade.isFinished {
				switch pinChangeFacade.validate() {
				case let .valid(pin):
					userSession.pinState = .created(pin: pin)
					refreshPinViewModel()
					coordinator?.pinCreateFinished(isCreated: true)
				case let .invalid(errorMessage):
					controller?.error(message: errorMessage)
					pinChangeFacade.resetToCreateStep()
					refreshPinViewModel()
				}
			} else {
				refreshPinViewModel()
			}
		} else {
			verifyPinIfNeeded()
			refreshPinViewModel()
		}
	}
}
