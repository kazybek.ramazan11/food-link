//
//  PinChangeFacade.swift
//  food_link
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import Foundation

private enum Constants {
	static let maxAttemptsCount = 5
}

typealias PinCreateStep = (title: String, pin: PinModel)

final class PinChangeFacade {
	enum VerificationResult {
		case valid
		case invalid(errorMessage: String)
		case rejected(message: String)
	}

	enum ValidationResult {
		case valid(pin: String)
		case invalid(errorMessage: String)
	}

	lazy var currentStep: PinCreateStep = currentPinStep

	private let currentPinStep = (title: "R.string.pinChange.currentPinCode()", pin: PinModel())
	private let createStep = (title: "R.string.pinChange.newPinCode()", pin: PinModel())
	private let verificationStep = (title: "R.string.pinChange.verifyPinCode()", pin: PinModel())

	var isFinished: Bool {
		currentPinStep.pin.isFilled && createStep.pin.isFilled && verificationStep.pin.isFilled
	}

	var isVerified: Bool = false

	var isPinForVerificiationFinished: Bool {
		currentPinStep.pin.isFilled
	}

	private var remainingAttemptsCount = Constants.maxAttemptsCount
	private let authPinValue: String?

	init(authPinValue: String?) {
		self.authPinValue = authPinValue
	}

	func changeStepIfNeeded() {
		if createStep.pin.isFilled {
			currentStep = verificationStep
		} else if currentPinStep.pin.isFilled {
			currentStep = createStep
		} else {
			currentStep = currentPinStep
		}
	}

	func push(_ digit: String) {
		guard !isFinished else { return }
		currentStep.pin.addDigit(digit)
	}

	@discardableResult
	func pop() -> String? {
		guard !isFinished else { return nil }
		return currentStep.pin.removeLastDigit()
	}

	func reset() {
		currentPinStep.pin.reset()
		createStep.pin.reset()
		verificationStep.pin.reset()
		changeStepIfNeeded()
	}

	func resetToCreateStep() {
		createStep.pin.reset()
		verificationStep.pin.reset()
		changeStepIfNeeded()
	}

	func validate() -> ValidationResult {
		if createStep.pin.value == verificationStep.pin.value {
			return .valid(pin: createStep.pin.value)
		} else {
			return .invalid(errorMessage: "R.string.pinCreate.pinMismatch()")
		}
	}

	func verify() -> VerificationResult {
		guard !isVerified else { return .valid }
		guard remainingAttemptsCount > 0 else { return .rejected(message: "R.string.pinVerify.pinRejected()") }
		decreaseAmountOfAttempts()
		isVerified = currentPinStep.pin.value == authPinValue
		return isVerified ? .valid : .invalid(errorMessage: "R.string.pinChange.currentPinIncorrect()")
	}

	private func decreaseAmountOfAttempts() {
		guard remainingAttemptsCount > 0 else { return }
		remainingAttemptsCount -= 1
	}
}
