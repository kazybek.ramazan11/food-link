//
//  PinChangeAssembly.swift
//  food_link
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import EasyDi
import FDFoundation

final class PinChangeAssembly: Assembly {
	private lazy var userSessionKit: UserSessionKit = DIContext.defaultInstance.assembly()
	
	func makePinChange(coordinator: PinChangeCoordinator) -> PinCreatePresentable {
		let interactor = PinChangeInteractor(userSession: userSessionKit.service)
		let controller = PinCreateViewController(interactor: interactor)
		return define(init: controller) {
			interactor.controller = controller
			interactor.coordinator = coordinator
			return $0
		}
	}
}
