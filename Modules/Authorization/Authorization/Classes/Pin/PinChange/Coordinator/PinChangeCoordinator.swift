//
//  PinChangeCoordinator.swift
//  food_link
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import EasyDi
import FDUIKit
import Foundation

protocol PinChangeCoordinatorOutput: class {
	var onFlowDidFinish: ((_ isCreated: Bool) -> Void)? { get set }
}

final class PinChangeCoordinator: Coordinator, PinChangeCoordinatorOutput {
	var onFlowDidFinish: ((_ isCreated: Bool) -> Void)?

	private let router: Router

	init(router: Router) {
		self.router = router
	}

	func start() {
		showPinChange()
	}

	func showPinChange() {
		let pinChangeAssembly: PinChangeAssembly = DIContext.defaultInstance.assembly()
		var pinChange = pinChangeAssembly.makePinChange(coordinator: self)
		pinChange.onBackButtonDidTap = { [weak self] in
			self?.onFlowDidFinish?(false)
		}
		pinChange.onBackDidSwipe = { [weak self] in
			self?.onFlowDidFinish?(false)
		}
		router.push(pinChange)
	}
	
	func pinCreateFinished(isCreated: Bool) {
		onFlowDidFinish?(isCreated)
	}
}
