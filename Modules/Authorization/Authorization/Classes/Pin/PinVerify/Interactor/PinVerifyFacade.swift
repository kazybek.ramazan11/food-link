//
//  PinVerifyFacade.swift
//  food_link
//
//  Created by Ramazan Kazybek on 4/29/21.
//

import FDUIKit

private enum Constants {
	static let maxAttemptsCount = 5
}

final class PinVerifyFacade {
	enum ValidationResult {
		case valid
		case invalid
		case rejected(message: String)
	}

	let pin = PinModel()

	var isFinished: Bool {
		pin.isFilled
	}

	private var remainingAttemptsCount = Constants.maxAttemptsCount
	private let authPinValue: String?

	init(authPinValue: String?) {
		self.authPinValue = authPinValue
	}

	func push(_ digit: String) {
		guard !isFinished else { return }
		pin.addDigit(digit)
	}

	@discardableResult
	func pop() -> String? {
		guard !isFinished else { return nil }
		return pin.removeLastDigit()
	}

	func reset() {
		pin.reset()
	}

	func fill(handler: Callback) {
		guard let authPinValue = authPinValue else { return }
		let splittedPin = Array(authPinValue)
		let filledDigits = pin.value.count
		reset()
		for (index, digit) in splittedPin.enumerated() {
			push(String(digit))
			if filledDigits < index + 1 {
				handler()
			}
		}
	}

	func validate() -> ValidationResult {
		decreaseAmountOfAttempts()
		guard remainingAttemptsCount > 0 else { return .rejected(message: "R.string.pinVerify.pinRejected()") }
		return pin.value == authPinValue ? .valid : .invalid
	}

	private func decreaseAmountOfAttempts() {
		guard remainingAttemptsCount > 0 else { return }
		remainingAttemptsCount -= 1
	}
}
