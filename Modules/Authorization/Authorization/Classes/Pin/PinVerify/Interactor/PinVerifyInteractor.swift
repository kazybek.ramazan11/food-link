//
//  PinVerifyInteractor.swift
//  food_link
//
//  Created by Ramazan Kazybek on 4/29/21.
//

import FDFoundation

final class PinVerifyInteractor: IPinInteractor {
	private let userSession: UserSession
	weak var controller: IPinViewController?
//	weak var coordinator: PinVerify
	private var pinVerifyFacade: PinVerifyFacade?
	private let biometricAuthService: BiometricAuthService
	private var viewModel: PinViewModel {
		PinViewModel(title: "R.string.pinVerify.pinTitle(user.firstName)", activeDotsCount: pinVerifyFacade!.pin.length)
	}
	
	init(userSession: UserSession, biometricAuthService: BiometricAuthService) {
		self.userSession = userSession
		self.biometricAuthService = biometricAuthService
		if case let UserPinState.created(pin) = userSession.pinState {
			self.pinVerifyFacade = PinVerifyFacade(authPinValue: pin)
		}
	}
	
	func refreshPinViewModel() {
		controller?.enter(viewModel: viewModel)
	}
	
	func didFillAllDots() {
		validatePinIfNeeded()
	}
	
	func didTapDigitButton(digit: String) {
		pinVerifyFacade?.push(digit)
		refreshPinViewModel()
	}
	
	func didTapDeleteButton() {
		pinVerifyFacade?.pop()
		refreshPinViewModel()
	}
	
	func didTapBiometricAuthButton() {
		
	}
	
	private func validatePinIfNeeded() {
		guard pinVerifyFacade?.isFinished ?? false else { return }
		switch pinVerifyFacade?.validate() {
		case .valid:
			pinVerifyFacade?.reset()
//			state = .pinVerified
		case .invalid:
			controller?.pinIncorrect()
			pinVerifyFacade?.reset()
			refreshPinViewModel()
		case let .rejected(message):
			pinVerifyFacade?.reset()
			controller?.error(message: message)
		default:
			break
		}
	}
	
	private func authenticateByBiometric() {
		biometricAuthService.authenticate(reason: getAuthenticationReason()).then { [weak self] in
			guard let self = self else { return }
			switch self.userSession.pinState {
			case .created:
				self.pinVerifyFacade?.fill {
					self.refreshPinViewModel()
				}
			default:
				break
			}
		}
	}
	
	private func getAuthenticationReason() -> String {
		switch biometricAuthService.type {
		case .none:
			return ""
		case .touchId:
			return "R.string.pinVerify.touchIDReason()"
		case .faceId:
			return "R.string.pinVerify.faceIDReason()"
		}
	}
}
