//
//  PinVerifyAssembly.swift
//  food_link
//
//  Created by Ramazan Kazybek on 4/29/21.
//

import EasyDi
import FDFoundation

final class PinVerifyAssembly: Assembly {
	private lazy var userSessionKit: UserSessionKit = DIContext.defaultInstance.assembly()
	private lazy var biometricAuthServiceKit: BiometricAuthServiceKit = DIContext.defaultInstance.assembly()
	
	func makePinVerifyCreate() -> PinCreatePresentable {
		let interactor = PinVerifyInteractor(userSession: userSessionKit.service, biometricAuthService: biometricAuthServiceKit.service)
		let controller = PinCreateViewController(interactor: interactor)
		return define(init: controller) {
			interactor.controller = controller
			return $0
		}
	}
}
