//
//  PinVerifyCoordinator.swift
//  food_link
//
//  Created by Ramazan Kazybek on 4/29/21.
//

import FDUIKit
import EasyDi

public protocol PinVerifyCoordinatorOutput: class {
	var onFlowDidFinish: ((_ isVerified: Bool) -> Void)? { get set }
}

final class PinVerifyCoordinator: Coordinator, PinVerifyCoordinatorOutput {
	var onFlowDidFinish: ((_ isVerified: Bool) -> Void)?
	private let router: Router
	
	init(router: Router) {
		self.router = router
	}

	func start() {
		showPinVerify()
	}

	func pinVerified() {
		onFlowDidFinish?(true)
	}
	
	func pinRejected() {
		onFlowDidFinish?(false)
	}
	
	private func showPinVerify() {
		let pinVerifyAssembly: PinVerifyAssembly = DIContext.defaultInstance.assembly()
		var pinVerify = pinVerifyAssembly.makePinVerifyCreate()
		pinVerify.onCloseDidTap = { [weak self] in
			self?.onFlowDidFinish?(false)
		}
		router.setRootModule(pinVerify)
	}
}
