//
//  PinViewController.swift
//  food_link
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import FDFoundation
import FDUIKit
import UIKit

class PinViewController: UIViewController {
	var biometricType: BiometricAuthType {
		get {
			pinView.biometricType
		}
		set {
			pinView.biometricType = newValue
		}
	}
	
	private var pinView = PinView()
	
	init() {
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		nil
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		view.backgroundColor = .white
		addSubviews()
		setupLayout()
		setupDelegate()
	}
	
	func configure(with viewModel: PinViewModel) {
		pinView.configure(with: viewModel)
	}
	
	func shakeAndVibrate() {
		pinView.shakeAndVibrate()
	}
	
	func setupDelegate() {
		pinView.delegate = self
	}
}

extension PinViewController: PinViewDelegate {
	@objc
	func pinView(_ pinView: PinView, didTapPinButton pinButton: PinButton) {}
	@objc
	func pinView(_ pinView: PinView, didFillDotsStackView dotsStackView: PinDotsStackView) {}
}

extension PinViewController: Decoratable {
	func addSubviews() {
		view.addSubview(pinView)
	}
	
	func setupLayout() {
		pinView.snp.makeConstraints { make in
			make.leading.trailing.equalToSuperview()
			make.centerY.equalTo(view.safeAreaLayoutGuide)
		}
	}
}
