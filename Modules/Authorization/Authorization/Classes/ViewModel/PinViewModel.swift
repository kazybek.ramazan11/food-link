//
//  PinViewModel.swift
//  food_link
//
//  Created by Ramazan Kazybek on 4/25/21.
//

struct PinViewModel {
	let title: String
	let activeDotsCount: Int
}
