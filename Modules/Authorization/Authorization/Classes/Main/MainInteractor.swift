//
//  MainInteractor.swift
//  Authorization
//
//  Created by iBol on 4/5/21.
//

import UIKit
import AuthorizationKit
import FDNetworkLayer

final class MainInteractor {
	// MARK: - Dependencies
	weak var controller: MainViewController?
	weak var coordinator: IMainCoordinatorOutput?
	private let service: IMainService
	
	public init(controller: MainViewController? = nil, coordinator: IMainCoordinatorOutput? = nil, service: IMainService) {
		self.controller = controller
		self.coordinator = coordinator
		self.service = service
	}
	
	func didTapContinueButton(with phoneNumber: String) {
		checkIsRegistered(with: phoneNumber)
	}
	
	func checkIsRegistered(with phoneNumber: String) {
		let model = MainModels.ValidatePhone.Request(phoneNumber: phoneNumber)
		service.validatePhone(with: .validatePhone(model)) { [weak self] result in
			DispatchQueue.main.async {
				switch result {
				case let .success(response):
					self?.continueFlow(with: response?.exists == 1, and: phoneNumber)
				case let .failure(error):
					print(error.localizedDescription)
				}
			}
		}
//		continueFlow(with: false, and: phoneNumber)
	}
	
	func navigateToAuth(with phoneNumber: String) {
		coordinator?.navigateToAuth(with: phoneNumber)
	}
	
	func navigateToRegister(with phoneNumber: String) {
		coordinator?.navigateToRegistration(with: phoneNumber)
	}
	
	private func continueFlow(with isExist: Bool?, and phoneNumber: String) {
		guard let isExist = isExist else { return }
		isExist ? navigateToAuth(with: phoneNumber) : navigateToRegister(with: phoneNumber)
	}
}
