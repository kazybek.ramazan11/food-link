//
//  MainAuthAssembly.swift
//  Authorization
//
//  Created by iBol on 5/2/21.
//

import EasyDi
import FDFoundation
import AuthorizationKit

final class MainAuthAssembly: Assembly {
	private let authorizationKitAssembly: AuthorizationKitAssembly = DIContext.defaultInstance.assembly()
	
	func makeMainAuth(coordinator: MainCoordinator) -> MainAuthPresentable {
		let interactor = MainInteractor(service: authorizationKitAssembly.service)
		let controller = MainViewController(interactor: interactor)
		return define(init: controller) {
			interactor.controller = controller
			interactor.coordinator = coordinator
			return $0
		}
	}
}
