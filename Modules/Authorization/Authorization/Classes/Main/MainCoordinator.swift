//
//  MainCoordinator.swift
//  Authorization
//
//  Created by iBol on 4/5/21.
//

import FDUIKit
import EasyDi

public typealias IMainCoordinatorOutput = (IMainCoordinator & IMainCoordinatorFlow & BaseCoordinator)

public protocol IMainCoordinatorFlow: AnyObject {}

public protocol IMainCoordinator: AnyObject {
	func navigateToAuth(with phoneNumber: String)
	func navigateToRegistration(with phoneNumber: String)
	func presentController(with controller: Presentable)
}

public final class MainCoordinator: IMainCoordinatorOutput, ExitCoordinator {
	// MARK: - Dependencies
	public var exitCompletion: Callback?
	
	// MARK: Init
	override public init(router: Router) {
		super.init(router: router)
	}
	
	override public func start() {
		navigateToMain()
	}
	
	public func navigateToMain() {
		let mainAuthAssembly: MainAuthAssembly = DIContext.defaultInstance.assembly()
		var mainAuth = mainAuthAssembly.makeMainAuth(coordinator: self)
		router.push(mainAuth)
	}
	
	public func navigateToAuth(with phoneNumber: String) {
		let authCoordinator = AuthCoordinator(router: router, phoneNumber: phoneNumber)
		authCoordinator.start()
	}
	
	public func navigateToRegistration(with phoneNumber: String) {
		let registrationCoordinator = RegistrationCoordinator(router: router, phoneNumber: phoneNumber)
		registrationCoordinator.start()
	}
	
	public func presentController(with controller: Presentable) {
		router.presentBottomSheet(controller)
	}
}
