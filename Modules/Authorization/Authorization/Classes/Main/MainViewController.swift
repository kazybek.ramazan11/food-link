//
//  MainViewController.swift
//  Authorization
//
//  Created by iBol on 4/3/21.
//

import FDUIKit

protocol MainAuthPresentable: Presentable {}

protocol IMainView: AnyObject {}

public class MainViewController: UIViewController, MainAuthPresentable {
	// MARK: Deps
	private let interactor: MainInteractor
	
	init(interactor: MainInteractor) {
		self.interactor = interactor
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	// MARK: - Lifecycle
	public override func loadView() {
		view = MainView(delegate: self)
	}
	
	override public func viewDidLoad() {
		super.viewDidLoad()
		
		view.backgroundColor = .white
	}
	
	public override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		navigationController?.isNavigationBarHidden = true
	}
	
	public override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		navigationController?.isNavigationBarHidden = false
	}
}

// MARK: MainViewDelegate
extension MainViewController: MainViewDelegate {
	func didTapContinueButton(with phoneNumber: String) {
		interactor.didTapContinueButton(with: phoneNumber)
	}
}
