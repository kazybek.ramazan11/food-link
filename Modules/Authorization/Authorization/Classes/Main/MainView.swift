//
//  MainView.swift
//  Authorization
//
//  Created by iBol on 4/25/21.
//

import FDUIKit
import IQKeyboardManager
import JMMaskTextField_Swift

protocol MainViewDelegate: AnyObject {
	func didTapContinueButton(with phoneNumber: String)
}

class MainView: UIView {
	// MARK: Deps
	public weak var delegate: MainViewDelegate?
	
	// MARK: - Views Factory
	private let logoImageView: UIImageView =
		UIImageViewFactory(image: UIImage.makeFromBundle(imageName: "logo-with-text"))
		.build()
	
	private let titleLabel: UILabel =
		UILabelFactory(text: "Вход/Регистрация")
		.font(FontFamily.Roboto.medium.font(size: 20))
		.text(color: .appBlack)
		.text(alignment: .center)
		.build()
	
	private let phoneNumberTitleLabel: UILabel =
		UILabelFactory(text: "Введите номер телефона")
		.font(FontFamily.Roboto.regular.font(size: 14))
		.text(color: .appGray)
		.build()
	
	private var phoneNumberTextField: TextFieldWithPadding = {
		let padding: UIEdgeInsets = .init(top: 7, left: 15, bottom: 7, right: 15)
		let shadowOffset = CGSize(width: 0, height: 4)
		let view = TextFieldWithPadding(padding: padding)
		view.backgroundColor = .fieldBackgroundGray
		(view as? JMMaskTextField)?.maskString = "+7 (000) 000 00 00"
		view.corner(radius: 7)
		view.addShadow(ofColor: .white, radius: 10, offset: shadowOffset, opacity: 0.1)
		view.placeholder = "+7(___) ___ __ __"
		view.font = FontFamily.Roboto.regular.font(size: 14)
		view.keyboardType = .numberPad
		return view
	}()
	
	private let continueButton: UIButton = {
		let view = UIButton.primaryButton(title: "Продолжить")
		let shadowOffset = CGSize(width: 0, height: 4)
//		view.isEnabled = false
		view.setBackgroundImage(UIColor.disabledGray.as1ptImage(), for: .disabled)
		view.addShadow(ofColor: .white, radius: 10, offset: shadowOffset, opacity: 0.1)
		view.tap(target: self, selector: #selector(didTapContinueButton))
		return view
	}()
	
	init(delegate: MainViewDelegate?) {
		super.init(frame: .zero)
		phoneNumberTextField.delegate = self
		phoneNumberTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
		self.delegate = delegate
		addSubviews()
		setupLayout()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	// MARK: - Actions
	@objc private func didTapContinueButton() {
		guard let phoneNumber = phoneNumberTextField.text,
			  !phoneNumber.isEmpty else { return }
		delegate?.didTapContinueButton(with: phoneNumber)
	}
	
	@objc func textFieldDidChange() {
		
	}
}

extension MainView: Decoratable {
	public func addSubviews() {
		[logoImageView, titleLabel, phoneNumberTitleLabel, phoneNumberTextField, continueButton].forEach { addSubview($0) }
	}
	
	public func setupLayout() {
		logoImageView.snp.makeConstraints { make in
			make.top.equalTo(safeAreaLayoutGuide).offset(22)
			make.centerX.equalToSuperview()
		}
		
		titleLabel.snp.makeConstraints { make in
			make.top.equalTo(logoImageView.snp.bottom).offset(70)
			make.centerX.equalToSuperview()
		}
		
		phoneNumberTitleLabel.snp.makeConstraints { make in
			make.top.equalTo(titleLabel.snp.bottom).offset(20)
			make.left.equalTo(phoneNumberTextField).offset(7)
		}
		
		phoneNumberTextField.snp.makeConstraints { make in
			make.top.equalTo(phoneNumberTitleLabel.snp.bottom).offset(7)
			make.left.equalToSuperview().offset(35)
			make.centerX.equalToSuperview()
			make.height.equalTo(48)
		}
		
		continueButton.snp.makeConstraints { make in
			make.bottom.equalTo(safeAreaLayoutGuide).inset(50)
			make.left.right.equalTo(phoneNumberTextField)
			make.height.equalTo(48)
		}
	}
}

extension MainView: UITextFieldDelegate {
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		print(phoneNumberTextField.text?.count)
		let isValidPhone = phoneNumberTextField.text?.count == 17
		continueButton.isEnabled = isValidPhone
		return true
	}
}
