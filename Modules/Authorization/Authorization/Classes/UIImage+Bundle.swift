//
//  UIImage+Bundle.swift
//  Authorization
//
//  Created by iBol on 4/3/21.
//

import FDUIKit

class BundleToken {}
extension UIImage {
	public static func makeFromBundle(imageName name: String) -> UIImage? {
		return UIImage.make(named: name, classForModuleBundle: BundleToken.self, bundleName: "Authorization-Assets")
	}
}
