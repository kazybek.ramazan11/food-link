//
//  AuthViewController.swift
//  Authorization
//
//  Created by iBol on 4/7/21.
//

import FDUIKit
import UIKit

protocol AuthPresentable: Presentable {}
protocol IAuthView: AnyObject {}

public class AuthViewController: UIViewController, AuthPresentable {
	// MARK: Deps
	private let interactor: AuthInteractor
	
	// MARK: Init
	init(interactor: AuthInteractor) {
		self.interactor = interactor
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	// MARK: - Lifecycle
	public override func loadView() {
		view = AuthView(delegate: self, phoneNumber: interactor.getPhoneNumber())
	}
	
	public override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		navigationController?.isNavigationBarHidden = true
	}
	
	public override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		navigationController?.isNavigationBarHidden = false
	}
}

// MARK: AuthViewDelegate
extension AuthViewController: AuthViewDelegate {
	func didTapContinueButton(with password: String) {
		interactor.auth(with: password)
	}
	
	func didOtpButtonTapped() {}
	
	func didBackButtonTapped() {
		navigationController?.popViewController(animated: true)
	}
}
