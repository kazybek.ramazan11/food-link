//
//  AuthAssembly.swift
//  Authorization
//
//  Created by iBol on 5/3/21.
//

import EasyDi
import FDFoundation
import AuthorizationKit

final class AuthAssembly: Assembly {
	private let authorizationKitAssembly: AuthorizationKitAssembly = DIContext.defaultInstance.assembly()
	
	func makeAuth(coordinator: AuthCoordinator, phoneNumber: String) -> AuthPresentable {
		let interactor = AuthInteractor(service: authorizationKitAssembly.service, phoneNumber: phoneNumber)
		let controller = AuthViewController(interactor: interactor)
		return define(init: controller) {
			interactor.controller = controller
			interactor.coordinator = coordinator
			return $0
		}
	}
}
