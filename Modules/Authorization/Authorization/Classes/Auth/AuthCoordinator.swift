//
//  AuthCoordinator.swift
//  Authorization
//
//  Created by iBol on 5/3/21.
//

import FDUIKit
import EasyDi

public typealias IAuthCoordinatorOutput = (IAuthCoordinator & IAuthCoordinatorFlow & BaseCoordinator)

public protocol IAuthCoordinatorFlow: AnyObject {}

public protocol IAuthCoordinator: AnyObject {
	func showOtp(with phoneNumber: String)
	func presentController(with controller: Presentable)
}

public final class AuthCoordinator: IAuthCoordinatorOutput, ExitCoordinator {
	// MARK: - Dependencies
	public var exitCompletion: Callback?
	private var phoneNumber: String
	
	// MARK: Init
	public init(router: Router, phoneNumber: String) {
		self.phoneNumber = phoneNumber
		super.init(router: router)
	}
	
	override public func start() {
		navigateToAuth()
	}
	
	public func navigateToAuth() {
		let authAssembly: AuthAssembly = DIContext.defaultInstance.assembly()
		var authModule = authAssembly.makeAuth(coordinator: self, phoneNumber: phoneNumber)
		router.push(authModule)
	}
	
	public func showOtp(with phoneNumber: String) {
		let otpCoordinator = OtpCoordinator(router: router, phoneNumber: phoneNumber)
		otpCoordinator.start()
	}
	
	public func presentController(with controller: Presentable) {
		router.presentBottomSheet(controller)
	}
}
