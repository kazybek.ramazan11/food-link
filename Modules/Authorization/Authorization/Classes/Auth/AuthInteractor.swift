//
//  AuthInteractor.swift
//  Authorization
//
//  Created by iBol on 5/3/21.
//

import UIKit
import AuthorizationKit
import FDNetworkLayer

final class AuthInteractor {
	// MARK: - Dependencies
	weak var controller: AuthViewController?
	weak var coordinator: IAuthCoordinatorOutput?
	private let service: IMainService
	private var phoneNumber: String
	
	public init(controller: AuthViewController? = nil, coordinator: IAuthCoordinatorOutput? = nil, service: IMainService, phoneNumber: String) {
		self.phoneNumber = phoneNumber
		self.controller = controller
		self.service = service
		self.coordinator = coordinator
	}
	
	public func getPhoneNumber() -> String {
		return phoneNumber
	}
	
	public func auth(with password: String) {
		let model = AuthModels.Auth.Request(phoneNumber: phoneNumber, password: password)
		service.auth(with: .auth(model)) { [weak self] result in
			DispatchQueue.main.async {
				switch result {
				case .success:
					self?.showOtp()
				case let .failure(error):
					print(error.localizedDescription)
				}
			}
		}
	}
	
	public func showOtp() {
		coordinator?.showOtp(with: phoneNumber)
	}
}
