//
//  PinModel.swift
//  food_link
//
//  Created by Ramazan Kazybek on 4/25/21.
//


final class PinModel {
	private(set) var value = ""

	var length: Int {
		value.count
	}

	var isFilled: Bool {
		value.count == 4
	}

	func addDigit(_ digit: String) {
		value += digit
	}

	@discardableResult
	func removeLastDigit() -> String? {
		value.isEmpty ? nil : String(value.removeLast())
	}

	func reset() {
		value.removeAll()
	}
}
