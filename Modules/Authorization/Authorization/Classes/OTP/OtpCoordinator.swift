//
//  OtpCoordinator.swift
//  Authorization
//
//  Created by iBol on 5/7/21.
//

import FDUIKit
import EasyDi

public typealias IOtpCoordinatorOutput = (IOtpCoordinator & IOtpCoordinatorFlow & BaseCoordinator)

public protocol IOtpCoordinatorFlow: AnyObject {}

public protocol IOtpCoordinator: AnyObject {}

public final class OtpCoordinator: IOtpCoordinatorOutput, ExitCoordinator {
	// MARK: - Variables
	private let phoneNumber: String

	// MARK: - Dependencies
	public var exitCompletion: Callback?
	
	// MARK: Init
	public init(router: Router, phoneNumber: String) {
		self.phoneNumber = phoneNumber
		
		super.init(router: router)
	}
	
	override public func start() {
		navigateToOtp()
	}
	
	public func navigateToOtp() {
		let otpAssembly: OtpAssembly = DIContext.defaultInstance.assembly()
		var otpViewController = otpAssembly.makeOtp(coordinator: self, phoneNumber: phoneNumber)
		router.push(otpViewController)
	}
	
	public func navigateToMain() {
		router.popToRootModule()
	}
}
