//
//  OtpAssembly.swift
//  Authorization
//
//  Created by iBol on 5/7/21.
//

import EasyDi
import FDFoundation
import AuthorizationKit

final class OtpAssembly: Assembly {
	private let authorizationKitAssembly: AuthorizationKitAssembly = DIContext.defaultInstance.assembly()
	
	func makeOtp(coordinator: OtpCoordinator, phoneNumber: String) -> OtpPresentable {
		let interactor = makeInteractor(phoneNumber: phoneNumber)
		let controller = OtpViewController(interactor: interactor)
		return define(init: controller) {
			interactor.controller = controller
			interactor.coordinator = coordinator
			return $0
		}
	}
	
	func makeInteractor(phoneNumber: String) -> OtpInteractor {
		return OtpInteractor(service: authorizationKitAssembly.service, phoneNumber: phoneNumber)
	}
}
