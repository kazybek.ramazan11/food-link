import AuthorizationKit
import FDNetworkLayer

protocol IOtpInteractor {
	func requestCode()
	func getPhoneNumber() -> String
	func verifyOtp(with code: String)
	func getDescriptionText() -> String
	func anotherPhoneNumberButtonDidPress()
}

public final class OtpInteractor: IOtpInteractor {
	
	public var phoneNumber: String
	public var password: String?
	public let loginType: LoginType = .phone
	
	private var descriptionText: String {
//		if phoneNumber.isValidPhone == true {
			return "Код подтверждения отправлен\n на номер \(String(describing: phoneNumber.userPresentablePhone ?? ""))"
//		}
//		return ""
	}
	
	public weak var controller: IOtpView?
	public var coordinator: OtpCoordinator?
	private let service: IMainService
	
	init(service: IMainService, phoneNumber: String) {
		self.phoneNumber = phoneNumber
		self.service = service
	}
	
	func getPhoneNumber() -> String { phoneNumber }
	func getDescriptionText() -> String { return descriptionText }
	
	func requestCode() {
		var model = AuthModels.SendOtp.Request(login: phoneNumber.formattedLogin)
		
		service.requestCode(with: .sendOtp(model)) { [weak self] (result) in
			switch result {
			case .success(let codeRequestResult):
				guard let response = codeRequestResult else { return }
				self?.controller?.sendOtpDidSuccess(with: response)
			case .failure(let error):
				self?.controller?.sendOtpDidFail(with: error)
			}
		}
	}
	
	func verifyOtp(with code: String) {
		let code = code.replacingOccurrences(of: " ", with: "")
		let model = AuthModels.VerifyOtp.Request(phoneNumber: phoneNumber, otpCode: code)
		
		service.verifyOtp(with: .verifyOtp(model)) { [weak self] result in
			switch result {
			case .success(let signinResult):
				guard let response = signinResult else { return }
				self?.controller?.verifyOtpDidSuccess(with: response)
			case .failure(let error):
				self?.controller?.verifyOtpDidFail(with: error)
			}
		}

//		let model = AuthModels.Signin.Request(grantType: .hashAndCode, login: login, password: password, hash: authHash, code: code.replacingOccurrences(of: " ", with: ""), userID: userID)
//
//		service.signIn(from: .signin(model)) { [weak self] (result) in
//			switch result {
//			case .success(let signinResult):
//				self?.presenter?.authDidSuccess(with: signinResult)
//			case .failure(let error):
//				self?.presenter?.authDidFail(with: error.localizedDescription)
//			}
//		}
	}
	
//	func fetchUserAndSave() {
//		guard UserDefaults.standard.token != nil else { return }
//		service.fetchPersonalInfo(with: .user) { [weak self] (result) in
//			switch result {
//			case .success(let value):
//				guard let value = value else { return }
//
//				self?.presenter?.fetchUserAndSaveDidSuccess(with: value)
//			case .failure(let networkError):
//				self?.presenter?.fetchUserAndSaveDidFail(with: networkError)
//			}
//		}
//	}
	
	public func anotherPhoneNumberButtonDidPress() {
//		coordinator.navigateToMain()
	}
}
