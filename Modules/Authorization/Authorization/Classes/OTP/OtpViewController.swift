//
//  OtpViewController.swift
//  Authorization
//
//  Created by iBol on 4/29/21.
//

import FDUIKit
import FDUtilities
import FDNetworkLayer

public enum PreviousScreen {
	case signUp
	case singIn
}

//protocol CodeDisplayLogic: class {
//	func codeRequestDidSuccess(with response: AuthModels.Code.Response?)
//	func codeRequestDidFail(with error: NetworkError?)
//
//	func authDidSuccess(with response: Token?)
//	func authDidFail(with error: String)
//
//	func fetchUserAndSaveDidSuccess(with user: User)
//	func fetchUserAndSaveDidFail(with error: String)
//
//	func viewDidLoad(loginType: LoginType, description: String)
//}

// MARK: - Global Properties
private var responderCurrentIndex = 0
private var secondsLeft = 0
private let oneCharacterLengthWithWhitespaces = 8
private let codeMaxLengthWithWhitespaces = 25
private let codeValidLength = 4

public protocol OtpPresentable: Presentable {}

public protocol IOtpView: AnyObject {
	func sendOtpDidSuccess(with response: AuthModels.SendOtp.Response)
	func sendOtpDidFail(with error: NetworkError)
	func verifyOtpDidSuccess(with response: AuthModels.VerifyOtp.Response)
	func verifyOtpDidFail(with error: NetworkError)
}

public class OtpViewController: UIViewController, OtpPresentable {
	// MARK: - Properties
	public var interactor: OtpInteractor
	public var previousScreen: PreviousScreen?
	public var timer: Timer?
	
	private let whiteSpaces = "       "
	private var codeDescription = ""
	
	// MARK: - Views
	private let otpView = OtpView()
	
	// MARK: - Lifecycle
	init(interactor: OtpInteractor) {
		self.interactor = interactor
		
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	public override func loadView() {
		view = otpView
	}
	
	public override func viewDidLoad() {
		super.viewDidLoad()
		
		delegating()
		addTargets()
		configureTimer()
		displayInfo()
	}
	
	public override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		navigationController?.navigationBar.changeBackgroundColor(to: .clear)
		registerKeyboardNotifications(onShow: { [weak self] (payload) in
			self?.otpView.onKeyboardAppear(with: payload.endFrame)
		}) { [weak self] (_) in
			self?.otpView.onKeyboardDisappear()
		}
	}
	
	public override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		otpView.codeTextField.becomeFirstResponder()
//		requestCode()
	}
	
	public override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		timer?.invalidate()
		unregisterKeyboardNotifications()
		navigationController?.navigationBar.changeBackgroundColor(to: .linkBlue)
	}
	
	private func delegating() {
		otpView.delegate = self
		otpView.codeTextField.delegate = self
	}
	
	private func addTargets() {
		otpView.codeTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
	}
	
	private func displayInfo() {
		otpView.displayDescription(with: interactor.getDescriptionText())
		otpView.displayResendButtonTitle(secondsLeft: nil)
		otpView.displayRetypeAnotherLoginTitle()
		otpView.layoutIfNeeded()
	}
	
	private func configureTimer() {
		secondsLeft = 0
		otpView.isResendButtonEnabled = false
		
		timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(countdown(_:)), userInfo: nil, repeats: true)
	}
	
	@objc private func countdown(_ timer: Timer) {
		secondsLeft += 1
		otpView.displayResendButtonTitle(secondsLeft: secondsLeft)
		
		if secondsLeft == 60 {
			timer.invalidate()
			otpView.displayResendButtonTitle(secondsLeft: nil)
			otpView.isResendButtonEnabled = true
		}
	}
	
	private func requestCode() {
		interactor.requestCode()
	}

	private func verifyOtp() {
		guard let code = otpView.codeTextField.text else {
			showError()
			return
		}

		showProgress()
		interactor.verifyOtp(with: code)
	}
}

// MARK: - UITextFieldDelegate
extension OtpViewController: UITextFieldDelegate {
	public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		guard let text = textField.text else {
			return false
		}

		if range.length + range.location > text.count {
			return false
		}

		if range.length == 1 && string.count == 0 {
			if text.count == codeMaxLengthWithWhitespaces {
				return true
			} else if text.count >= oneCharacterLengthWithWhitespaces {
				textField.text = text.substring(to: text.count - oneCharacterLengthWithWhitespaces)
				textFieldDidChange(textField)
			} else {
				textField.text = nil
			}
		}

		let maxLength = text.count + string.count - range.length
		return maxLength <= codeMaxLengthWithWhitespaces
	}

	public func textFieldDidBeginEditing(_ textField: UITextField) {
		let index = Int(Float(textField.text?.count ?? 0) / Float(oneCharacterLengthWithWhitespaces))
		otpView.selectBorderColor(at: index)
	}

	public func textFieldDidEndEditing(_ textField: UITextField) {
		let digitsWithoutWhitespaces = textField.text?.replacingOccurrences(of: " ", with: "")
		let index = digitsWithoutWhitespaces?.count ?? 0
		otpView.selectBorderColor(at: index - 1)
	}

	@objc public func textFieldDidChange(_ textField: UITextField) {
		let digitsWithoutWhitespaces = textField.text?.replacingOccurrences(of: " ", with: "") ?? ""

		var result = ""

		if digitsWithoutWhitespaces.count == codeValidLength {
			otpView.selectBorderColor(at: 3)
			otpView.codeTextField.resignFirstResponder()
			verifyOtp()
		} else {
			otpView.selectBorderColor(at: digitsWithoutWhitespaces.count)
		}

		for i in 0 ..< digitsWithoutWhitespaces.count {
			let index = digitsWithoutWhitespaces.index(digitsWithoutWhitespaces.startIndex, offsetBy: i)
			let digit = "\(digitsWithoutWhitespaces[index])"
			result.append("\(digit)\(whiteSpaces)")
		}

		if result.count > codeMaxLengthWithWhitespaces {
			let index = result.index(result.startIndex, offsetBy: codeMaxLengthWithWhitespaces)
			result = String(result[..<index])
		}

		textField.text = result
	}
}

//    MARK: - CodeViewControllable
//extension CodeViewController: CodeDisplayLogic {
//	func codeRequestDidSuccess(with response: AuthModels.Code.Response?) {
//		if !otpView.codeTextField.isFirstResponder {
//			codeView.codeTextField.becomeFirstResponder()
//		}
//	}
//
//	func codeRequestDidFail(with error: NetworkError?) {
//		guard let error = error, let data = error.data, let remainingAt = data["remaining_at"] as? Int  else {
//			navigationController?.popViewController(animated: true)
//			return
//		}
//
//		showError(with: error.localizedDescription)
//		otpView.endEditing(true)
//		secondsLeft = 60 - remainingAt
//		guard let loginType = loginType else { return }
//		otpView.displayResendButtonTitle(with: loginType, and: secondsLeft)
//	}
//
//	func authDidSuccess(with response: Token?) {
//		guard let response = response else { return }
////        AuthMetrics.authConfirmCompleted()
//		response.save()
//
//		if let previousScreen = previousScreen {
//			switch previousScreen {
//			case .signIn:
////                AuthMetrics.authSignInCompleted()
////                AuthMetrics.authCompleted()
//				FirebaseService().getAndSynchronizePushToken(shouldForceSync: true)
//				break
//			case .signUp:
////                AuthMetrics.authSignInCompleted()
////                AuthMetrics.authRegisterSuccess()
//				FirebaseService().getAndSynchronizePushToken(shouldForceSync: true)
//				break
//			}
//		}
//
//		interactor?.fetchUserAndSave()
//	}
//
//	func authDidFail(with error: String) {
//		dismissProgress()
//		showError(with: error)
//	}
//
//	func fetchUserAndSaveDidSuccess(with user: User) {
//		dismissProgress()
//		UserDefaults.standard.userProfile = user
//		NotificationCenter.default.post(name: .userDidSignin, object: nil)
//
//		router?.routeTo(destination: .main)
//		UserDefaults.standard.removeObject(forKey: "shouldSendSenderType")
//	}
//
//	func fetchUserAndSaveDidFail(with error: String) {
//		dismissProgress()
//		NotificationCenter.default.post(name: .userDidSignin, object: nil)
//
//		router?.routeTo(destination: .main)
//	}
//
//	func viewDidLoad(loginType: LoginType, description: String) {
//		self.loginType = loginType
//		self.codeDescription = description
//		displayInfo(with: loginType)
//	}
//}

// MARK: - OtpViewDelegate
extension OtpViewController: OtpViewDelegate {
	public func resendButtonDidPress() {
		if secondsLeft == 60 {
			configureTimer()
//			requestCode()
		}
	}
	
	public func anotherPhoneNumberButtonDidPress() {
		interactor.anotherPhoneNumberButtonDidPress()
	}
}

// MARK: - IOtpView
extension OtpViewController: IOtpView {
	public func sendOtpDidSuccess(with response: AuthModels.SendOtp.Response) {
		if !otpView.codeTextField.isFirstResponder {
			otpView.codeTextField.becomeFirstResponder()
		}
	}
	
	public func sendOtpDidFail(with error: NetworkError) {
		guard let data = error.data, let remainingAt = data["remaining_at"] as? Int  else {
			navigationController?.popViewController(animated: true)
			return
		}

		showError(message: error.localizedDescription)
		otpView.endEditing(true)
		secondsLeft = 60 - remainingAt
		otpView.displayResendButtonTitle(secondsLeft: secondsLeft)
	}
	
	public func verifyOtpDidSuccess(with response: AuthModels.VerifyOtp.Response) {}
	public func verifyOtpDidFail(with error: NetworkError) {
		dismissProgress()
		showError(message: error.localizedDescription)
	}
}

