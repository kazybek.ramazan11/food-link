//
//  OtpView.swift
//  Authorization
//
//  Created by iBol on 4/29/21.
//

import UIKit
import FDUIKit

protocol OtpViewDelegate: AnyObject {
	func resendButtonDidPress()
	func anotherPhoneNumberButtonDidPress()
}

class OtpView: UIView {
	
	// MARK: - Views
	private let containerView = UIView()
	
	private let titleLabel: UILabel =
		UILabelFactory(text: "Введите код из СМС".uppercased())
			.font(FontFamily.Roboto.regular.font(size: 18))
			.text(color: .appBlack)
			.text(alignment: .center)
			.build()
	
	private let descriptionLabel: UILabel =
		UILabelFactory(text: "")
			.font(FontFamily.Roboto.regular.font(size: 14))
			.text(color: .appBlack)
			.numberOf(lines: 2)
			.text(alignment: .center)
			.build()
	
	private let codeContainerView = UIView()
	
	private let bordersStackView: UIStackView = {
		let bordersStackView = UIStackView()
		bordersStackView.axis = .horizontal
		bordersStackView.alignment = .fill
		bordersStackView.distribution = .fillEqually
		bordersStackView.spacing = 8
		
		return bordersStackView
	}()
	
	private let borderViews: [UIView] = {
		let views = Array(0 ... 3).map { (_) -> UIView in
			let view = UIView()
			view.backgroundColor = .borderGrey
			view.layer.cornerRadius = 2
			
			return view
		}
		
		return views
	}()
	
	public let codeTextField: UITextField =
		UITextFieldFactory()
			.font(size: 24, minimumSize: 17)
			.keyboard(Type: .numberPad)
			.border(style: .none)
			.build()
	
//    public let codeTextField: UITextField = {
//        let textField = UITextField()
//        textField.font = UIFont.systemFont(ofSize: 24.0)
//        textField.borderStyle = .none
//        textField.minimumFontSize = 17.0
//        textField.keyboardType = .numberPad
//        if #available(iOS 12.0, *) {
//            textField.textContentType = .oneTimeCode
//        }
//
//        return textField
//    }()
	
	private lazy var resendButton: UIButton =
		UIButtonFactory(type: .custom, title: "", style: .underlined, color: .interfaceGray)
			.font(size: 14)
			.addTarget(self, action: #selector(handleResendButtonDidPress(_:)))
			.build()

	
	private lazy var anotherPhoneNumberButton: UIButton =
		UIButtonFactory(type: .system, style: .underlined, color: .interfaceGray)
			.font(size: 14)
			.addTarget(self, action: #selector(handleAnotherPhoneNumberButtonDidPress(_:)))
			.build()
	
	// MARK: - Properties
	public weak var delegate: OtpViewDelegate?
	
	public var isResendButtonEnabled = false {
		didSet {
			if isResendButtonEnabled {
				enableResendButton()
			} else {
				disableResendButton()
			}
		}
	}
	
	// MARK: - Inits
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		backgroundColor = .white
        if #available(iOS 12.0, *) {
            codeTextField.textContentType = .oneTimeCode
        }
		addSubviews()
		setupLayout()
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError()
	}
	
	public func onKeyboardAppear(with endFrame: CGRect) {
		containerView.snp.remakeConstraints {
			$0.top.equalTo(-30)
			$0.leading.equalToSuperview()
			$0.trailing.equalToSuperview()
		}
	}
	
	public func onKeyboardDisappear() {
		containerView.snp.remakeConstraints {
			$0.top.equalTo(50)
			$0.leading.equalToSuperview()
			$0.trailing.equalToSuperview()
		}
	}
	
	public func displayDescription(with description: String) {
		descriptionLabel.text = description
	}
	
	public func displayResendButtonTitle(secondsLeft: Int?) {
		let resendButtonDefaultTitle = "Повторно отправить СМС"
		
		guard let secondsLeft = secondsLeft else {
			resendButton.setTitle(resendButtonDefaultTitle, for: .normal)
			return
		}
		
		resendButton.setTitle(String(format: "%@ через %02d:%02d", resendButtonDefaultTitle, secondsLeft / 60, 60 - secondsLeft), for: .normal)
	}
	
	public func displayRetypeAnotherLoginTitle() {
		
		anotherPhoneNumberButton.setTitle("Ввести другой номер телефона", for: .normal)
		anotherPhoneNumberButton.underline()
	}
	
	public func setDescriptionText(_ description: String) {
		descriptionLabel.text = description
	}
	
	private func enableResendButton() {
		resendButton.setTitleColor(anotherPhoneNumberButton.titleColor(for: .normal), for: .normal)
		resendButton.isUserInteractionEnabled = true
	}
	
	private func disableResendButton() {
		resendButton.setTitleColor(.interfaceGray, for: .normal)
		resendButton.isUserInteractionEnabled = false
	}
	
	public func selectBorderColor(at index: Int) {
		UIView.animate(withDuration: 0.3) { [weak self] in
			guard let self = self else { return }
			
			for iIndex in stride(from: 0, through: index, by: 1) {
				self.borderViews[iIndex].backgroundColor = UIColor.appOrange
			}
			
			for jIndex in stride(from: self.borderViews.count - 1, to: index, by: -1) {
				self.borderViews[jIndex].backgroundColor = UIColor.borderGrey
			}
		}
	}
	
	@objc private func handleResendButtonDidPress(_ sender: UIButton) {
		delegate?.resendButtonDidPress()
	}
	
	@objc private func handleAnotherPhoneNumberButtonDidPress(_ sender: UIButton) {
		delegate?.anotherPhoneNumberButtonDidPress()
	}
}

//    MARK: - Decoratable
extension OtpView: Decoratable {
	func addSubviews() {
		containerView.addSubviews([
			titleLabel,
			descriptionLabel,
			codeContainerView,
			resendButton,
			anotherPhoneNumberButton
		])
		
		codeContainerView.addSubviews([
			bordersStackView,
			codeTextField
		])
		
		for borderView in borderViews {
			bordersStackView.addArrangedSubview(borderView)
		}
		
		addSubviews([
			containerView
		])
	}
	
	func setupLayout() {
		containerView.snp.makeConstraints {
			$0.top.equalTo(50)
			$0.leading.equalToSuperview()
			$0.trailing.equalToSuperview()
		}
		
		titleLabel.snp.makeConstraints {
			$0.top.equalTo(containerView.snp.top)
			$0.leading.equalTo(containerView.snp.leading).offset(8)
			$0.trailing.equalTo(containerView.snp.trailing).offset(-8)
			$0.height.equalTo(27)
		}
		
		descriptionLabel.snp.makeConstraints {
			$0.top.equalTo(titleLabel.snp.bottom).offset(20)
			$0.leading.equalTo(titleLabel.snp.leading).offset(8)
			$0.trailing.equalTo(titleLabel.snp.trailing).offset(-8)
			$0.height.equalTo(33.5)
		}
		
		codeContainerView.snp.makeConstraints {
			$0.top.equalTo(descriptionLabel.snp.bottom).offset(20)
			$0.width.equalTo(187)
			$0.height.equalTo(40)
			$0.centerX.equalToSuperview()
		}
		
		bordersStackView.snp.makeConstraints {
			$0.leading.equalToSuperview()
			$0.trailing.equalToSuperview()
			$0.bottom.equalToSuperview()
			$0.height.equalTo(4)
		}
		
		codeTextField.snp.makeConstraints {
			$0.leading.equalToSuperview().offset(8)
			$0.top.equalToSuperview()
			$0.bottom.equalToSuperview()
			$0.trailing.equalToSuperview()
		}
		
		resendButton.snp.makeConstraints {
			$0.top.equalTo(codeContainerView.snp.bottom).offset(20)
			$0.centerX.equalToSuperview()
		}
		
		anotherPhoneNumberButton.snp.makeConstraints {
			$0.top.equalTo(resendButton.snp.bottom).offset(16)
			$0.centerX.equalToSuperview()
			$0.bottom.equalToSuperview()
		}
	}
}
