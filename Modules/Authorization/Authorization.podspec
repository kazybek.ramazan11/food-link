#
# Be sure to run `pod lib lint Authorization.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Authorization'
  s.version          = '0.1.0'
  s.summary          = 'A short description of Authorization.'
  s.homepage         = 'https://github.com/iBol/Authorization'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'iBol' => 'aibolseed@gmail.com' }
  s.source           = { :git => 'https://github.com/iBol/Authorization.git', :tag => s.version.to_s }
  s.ios.deployment_target = '12.0'
	s.source_files = 'Authorization/Classes/**/*'
	s.resource_bundles = { 'Authorization-Assets' => ['Authorization/Assets/**'] }
	s.dependency 'AuthorizationKit'
	s.dependency 'EasyDi'
	s.dependency 'SnapKit'
	s.dependency 'KeyValueStorage'
	s.dependency 'FDUIKit'
	s.dependency 'IQKeyboardManager'
	s.dependency 'JMMaskTextField-Swift'

  s.source_files = 'Authorization/Classes/**/*'
end
