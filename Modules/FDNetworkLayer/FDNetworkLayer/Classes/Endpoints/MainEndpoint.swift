//
//  MainEndpoint.swift
//  FDNetworkLayer
//
//  Created by iBol on 5/7/21.
//

import Foundation

public enum MainEndpoint {
	case validatePhone(MainModels.ValidatePhone.Request)
	case register(AuthModels.Register.Request)
	case auth(AuthModels.Auth.Request)
	case sendOtp(AuthModels.SendOtp.Request)
	case verifyOtp(AuthModels.VerifyOtp.Request)
}

extension MainEndpoint: Endpoint {
	public var path: String {
		switch self {
		case .validatePhone:
			return "/api/v1/auth/validate_phone/"
		case .register:
			return "/api/v1/auth/registration/"
		case .auth:
			return "/api/v1/auth/"
		case .sendOtp:
			return "/api/v1/auth/send_otp/"
		case .verifyOtp:
			return "/api/v1/auth/verify_otp/"
		}
	}
	
	public var headers: HTTPHeaders {
		switch self {
		case .validatePhone, .sendOtp, .verifyOtp, .register, .auth:
			return .default
		}
	}
	
	public var method: HTTPMethod {
		switch self {
		case .validatePhone:
			return .put
		case .sendOtp, .verifyOtp, .register, .auth:
			return .post
		}
	}
	
	public var body: BodyParameters {
		switch self {
		case let .validatePhone(request):
			return [
				"phone": request.phoneNumber
			]
		case let .register(request):
			return [
				"phone": request.phone,
				"password": request.password
			]
		case let .auth(request):
			return [
				"phone": request.phone,
				"password": request.password
			]
		case let .sendOtp(request):
			return [
				"phone": request.login
			]
		case let .verifyOtp(request):
			return [
				"phone": request.phoneNumber,
				"otp_code": request.otpCode
			]
		}
	}
	
	public var parameters: RequestParameters? {
		switch self {
		case .validatePhone, .sendOtp, .verifyOtp, .register, .auth:
			return nil
		}
	}
}
