//
//  MainModels.swift
//  FDNetworkLayer
//
//  Created by iBol on 5/7/21.
//

import Foundation

public enum MainModels {
	public enum ValidatePhone {
		public struct Request: Codable {
			public let phoneNumber: String
			
			public init(phoneNumber: String) {
				self.phoneNumber = phoneNumber
			}
		}
	}
}
