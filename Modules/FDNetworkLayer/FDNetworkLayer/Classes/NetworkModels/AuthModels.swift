//
//  AuthModels.swift
//  FDNetworkLayer
//
//  Created by iBol on 5/8/21.
//

//
//  AuthModels.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 25.08.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import Foundation

public enum GrantType: String, Codable {
	case password
	case hashAndCode = "hash_and_code"
	case authCode = "authorization_code"
	case refreshToken = "refresh_token"
}

public enum LoginType: String, Codable {
	case phone
	
	public var title: String {
		return "Введите код из СМС".uppercased()
	}
	
	public var resendButtonTitle: String {
		return "Повторно отправить СМС"
	}
	
	public var retypeAnotherLoginTitle: String {
		return "Ввести другой номер телефона"
	}
}

public enum AuthModels {
	public struct Validation {
		struct Request: Encodable {
			public let login: String
		}
		
		public struct Response: Codable {
//			let exist: Int
//			let status: Int
//
//			var isRegistered: Bool {
//				return exist == 1 && status == 1
//			}
//
//			private enum CodingKeys: String, CodingKey {
//				case exist
//				case status
//				case noticed
//			}
		}
	}
	
	public struct Register {
		public struct Request: Encodable {
			public let phone: String
			public let password: String
			
			public init(phoneNumber: String, password: String) {
				self.phone = phoneNumber
				self.password = password
			}
		}
		
		public struct Response: Codable {}
	}
	
	public struct Auth {
		public struct Request: Encodable {
			public let phone: String
			public let password: String
			
			public init(phoneNumber: String, password: String) {
				self.phone = phoneNumber
				self.password = password
			}
		}
		
		public struct Response: Codable {}
	}
	
	public struct Signin {
		public struct Request: Encodable {
//			let clientID: Int
			let grantType: GrantType?
			let login: String?
			let password: String?
//			let hash: String?
//			let code: String?
//			let redirectURI: String?
//			let userID: String?
//
//			private enum CodingKeys: String, CodingKey {
//				case clientID = "client_id"
//				case grantType = "grant_type"
//				case login
//				case password
//				case hash
//				case code
//				case redirectURI = "redirect_uri"
//				case userID = "user_id"
//			}
//
			init(grantType: GrantType? = nil, login: String? = nil, password: String? = nil) {
				self.grantType = grantType
				self.login = login
				self.password = password
			}
		}
		
		struct Response { }
	}
	
	struct Signup {
		struct Request: Encodable {
//			let grantType: GrantType
//			let phone: String
//			let password: String
//
//			private enum CodingKeys: String, CodingKey {
//				case clientID = "client_id"
//				case grantType = "grant_type"
//				case phone
//				case password
//			}
		}
		
		public struct Response: Codable {
			let expireIn: Int64
			let hash: String
			
			private enum CodingKeys: String, CodingKey {
				case expireIn = "expire_in"
				case hash
			}
		}
	}
	
	public struct SendOtp {
		public struct Request: Codable {
			let login: String?
//			let password: String?
//
//			private enum CodingKeys: String, CodingKey {
//				case login
//				case clientID = "client_id"
//				case loginType = "login_type"
//				case userID = "user_id"
//				case hash
//				case password
//			}
//
			public init(login: String? = nil) {
				self.login = login
//				self.password = password
			}
		}
		
		public struct Response: Codable {
			let expireIn: Int64
			let hash: String
			
			private enum CodingKeys: String, CodingKey {
				case expireIn = "expire_in"
				case hash
			}
		}
	}
	
	public struct VerifyOtp {
		public struct Request: Codable {
			let phoneNumber: String?
			let otpCode: String?
//			let password: String?
//
//			private enum CodingKeys: String, CodingKey {
//				case login
//				case clientID = "client_id"
//				case loginType = "login_type"
//				case userID = "user_id"
//				case hash
//				case password
//			}
//
			public init(phoneNumber: String? = nil, otpCode: String? = nil) {
				self.phoneNumber = phoneNumber
				self.otpCode = otpCode
//				self.password = password
			}
			
			public struct Response: Codable {}
		}
		
		public struct Response: Codable {
			let expireIn: Int64
			let hash: String
			
			private enum CodingKeys: String, CodingKey {
				case expireIn = "expire_in"
				case hash
			}
		}
	}
	
	public struct RefreshTokens {
		public struct Request: Codable {
//			let clientID: Int
//			let grantType: GrantType
//			let refreshToken: String
//
//			init(refreshToken: String) {
//				self.clientID = Environment.clientId
//				self.grantType = .refreshToken
//				self.refreshToken = refreshToken
//			}
//
//			private enum CodingKeys: String, CodingKey {
//				case clientID = "client_id"
//				case grantType = "grant_type"
//				case refreshToken = "refresh_token"
//			}
		}
		
		public struct Response { }
	}
}
