//
//  NetworkRequest.swift
//  FDNetworkLayer
//
//  Created by iBol on 5/7/21.
//

import Foundation

public protocol INetworkRequest {
	associatedtype RequestModel: Encodable
	associatedtype ResponseModel: Decodable
	
	var path: String { get }
	var method: NetworkMethod { get }
	var body: RequestModel { get }
	var queryItems: [URLQueryItem]? { get }
	var isAuth: Bool? { get }
	var shouldSendAntiFraudParams: Bool? { get }
}

public extension INetworkRequest {
	var queryItems: [URLQueryItem]? {
		return nil
	}
	
	var isAuth: Bool? {
		return true
	}
	
	var shouldSendAntiFraudParams: Bool? {
		return true
	}
}
