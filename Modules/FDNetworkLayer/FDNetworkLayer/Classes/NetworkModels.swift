//
//  NetworkModels.swift
//  FDNetworkLayer
//
//  Created by iBol on 4/25/21.
//

import UIKit
import FDUtilities

public typealias RequestParameters = [String: String]
public typealias BodyParameters = [String: Any]

public enum HTTPMethod: String {
	case options = "OPTIONS"
	case get     = "GET"
	case head    = "HEAD"
	case post    = "POST"
	case put     = "PUT"
	case patch   = "PATCH"
	case delete  = "DELETE"
	case trace   = "TRACE"
	case connect = "CONNECT"
}

public enum clientId: Int {
	case dev
}

public typealias HTTPHeaders = [String: String]

extension HTTPHeaders {
	static var `default`: Self {
		let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
		let uuid = UIDevice.current.identifierForVendor?.uuidString
//        let lastCoordinate = LocationManager.shared.getLastUpdatedLocation() ?? CLLocationCoordinate2DMake(0.0, 0.0)
		var headers = [
			"X-App-Locale": "ru_KZ",
			"X-App-Version": appVersion ?? "",
			"X-Device-Id": uuid ?? "",
//            "X-Device-Location": "\(lastCoordinate.latitude)-\(lastCoordinate.longitude)",
			"X-OS": "iOS",
			"Content-Type": "application/x-www-form-urlencoded"
		]
//        TODO: - refactor
		if UserDefaults.standard.bool(forKey: "shouldSendSenderType") {
			headers["X-Email-Sender-Type"] = "1"
		} else {
			headers.removeValue(forKey: "X-Email-Sender-Type")
		}
		
		return headers
	}
	
	static var auth: Self {
		var headers = HTTPHeaders.default
		guard let authToken = UserDefaults.standard.token?.authToken else {
			return headers
		}
		
		headers["Authorization"] = "Bearer \(authToken)"
		
		return headers
	}
	
	static var signIn: Self {
		var headers = HTTPHeaders.default
		let fingerprint = UUID().uuidString
		
		headers["X-Fingerprint"] = fingerprint
		
		return headers
	}
	
	static var imageData: Self {
		var headers = HTTPHeaders.auth
		headers["Content-Type"] = "image/jpeg"
		
		return headers
	}
	
	static var sender: Self {
		var headers = HTTPHeaders.default
		headers["X-Email-Sender-Type"] = "1"
		
		return headers
	}
	
	static var duplex: Self {
		if UserDefaults.standard.token == nil {
			return HTTPHeaders.default
		} else {
			return HTTPHeaders.auth
		}
	}
	
//	static var xUser: Self {
//		var headers = HTTPHeaders.auth
//		guard let user = UserDefaults.standard.userProfile else { return headers }
//		headers["X-User"] = String(user.id)
//		
//		return headers
//	}
}
