//
//  BaseResponse.swift
//  FDUtilities
//
//  Created by iBol on 4/25/21.
//

import Foundation

public typealias ResponseClosure<T: Codable> = ((BaseResponse<T>?, Error?) -> Void)

public struct BaseResponse<T: Codable>: Codable {
	public let errorCode: Int
	public let message: String
	public let status: String
	public let data: T?
	
	public init(errorCode: Int, message: String, status: String, data: T?) {
		self.errorCode = errorCode
		self.message = message
		self.status = status
		self.data = data
	}
	
	private enum CodingKeys: String, CodingKey {
		case errorCode = "error_code"
		case message
		case status
		case data
	}
	
}

//struct PaginationResponse<T: Codable>: Codable {
//    let items: [T]
//    let currentPage: StringType<Int>
//    let totalPages: StringType<Int>
//    let itemsCount: StringType<Int>
//
//    var isLastPage: Bool {
//        if totalPages.value == 0 {
//            return true
//        }
//
//        return currentPage.value >= totalPages.value
//    }
//
//    private enum CodingKeys: String, CodingKey {
//        case items
//        case currentPage = "current_page"
//        case totalPages = "total_pages"
//        case itemsCount = "items_count"
//    }
//}

public struct EmptyModel: Codable {
	
}

public struct TokenModel: Codable {
	let token: String
}
