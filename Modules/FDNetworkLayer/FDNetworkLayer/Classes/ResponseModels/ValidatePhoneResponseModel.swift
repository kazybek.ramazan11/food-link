//
//  ValidatePhoneResponseModel.swift
//  FDNetworkLayer
//
//  Created by iBol on 5/7/21.
//

import Foundation

public struct ValidatePhoneResponseModel: Codable {
	public let exists: Int?
	
	public init(from decoder: Decoder) throws {
		var container = try decoder.singleValueContainer()
		self.exists = try container.decode(Int.self)
	}
}
