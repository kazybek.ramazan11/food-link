//
//  NetworkAssembly.swift
//  FDNetworkLayer
//
//  Created by iBol on 5/3/21.
//

import EasyDi
import StorageKit

public final class NetworkAssembly: Assembly {
	// MARK: - Deps
	private lazy var storagesAssembly: StoragesAssembly = self.context.assembly()
	
	// MARK: - Public
	public func isUserAuthorized() -> Bool {
		return appTokenService.accessToken != nil
			&& appTokenService.refreshToken != nil
	}
	
	public var appTokenService: INetworkTokenService {
		return define(scope: Scope.lazySingleton, init: NetworkTokenService(self.storagesAssembly.keychainStorage))
	}
	
	public func removeTokens() {
		appTokenService.removeTokens()
	}
}
