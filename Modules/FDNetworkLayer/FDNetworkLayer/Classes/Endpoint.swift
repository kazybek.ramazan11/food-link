//
//  Endpoint.swift
//  FDNetworkLayer
//
//  Created by iBol on 4/25/21.
//

import Foundation
import FDUtilities

public protocol Endpoint {
	var scheme: String { get }
	var host: String { get }
	var path: String { get }
	var headers: HTTPHeaders { get }
	var parameters: RequestParameters? { get }
	var body: BodyParameters { get }
	var method: HTTPMethod { get }
}

extension Endpoint {
	
	public var urlComponents: URLComponents {
		let queryItems = parameters?.compactMap {
			return URLQueryItem(name: $0.0, value: $0.1)
		}
		
		var components = URLComponents(string: host)!
		components.scheme = scheme
		components.host = host
		components.path = path
		components.queryItems = queryItems
		
		return components
	}
	
	public var request: URLRequest {
		let url = urlComponents.url!
		var urlRequest = URLRequest(url: url)
		urlRequest.httpMethod = method.rawValue
		urlRequest.httpBody = body.percentEscaped().data(using: .utf8)
		
		headers.forEach { (key, value) in
			urlRequest.setValue(value, forHTTPHeaderField: key)
		}
		
		return urlRequest
	}
	
	public var description: String {
		return """
		\n
		-------------- ENDPOINT --------------
		HOST: \(host)
		PATH: \(path)
		HEADERS: \(headers)
		PARAMETERS: \(parameters.debugDescription)
		BODY: \(body.debugDescription)
		METHOD: \(method)
		--------------------------------------
		"""
	}
}

extension Endpoint {
	
	public var scheme: String {
		return "https"
	}
	
	public var host: String {
		return "foodlinkback.herokuapp.com"
	}
}

