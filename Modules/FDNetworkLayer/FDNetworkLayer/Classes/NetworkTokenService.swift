//
//  NetworkTokenService.swift
//  FDNetworkLayer
//
//  Created by iBol on 5/3/21.
//

import Foundation
import KeychainSwift

public protocol INetworkTokenService: AnyObject {
	var accessToken: String? { get set }
	var refreshToken: Data? { get set }
	func removeTokens()
}

public final class NetworkTokenService: INetworkTokenService {
	private let appTokenKey = "TOKEN"
	private let appRefreshToken = "REFRESH_TOKEN"
	private let keychainStorage: KeychainSwift
	init(_ keychainStorage: KeychainSwift) {
		self.keychainStorage = keychainStorage
	}
	
	public var accessToken: String? {
		get {
			guard let value = keychainStorage.get(appTokenKey) else {
				return nil
			}
			return value
		}
		
		set {
			if let value = newValue {
				keychainStorage.set(value, forKey: appTokenKey)
			}
		}
	}
	
	public var refreshToken: Data? {
		get {
			guard let value = keychainStorage.getData(appRefreshToken) else {
				return nil
			}
			return value
		}
		set {
			if let value = newValue {
				keychainStorage.set(value, forKey: appRefreshToken)
			}
		}
	}
	
	public func removeTokens() {
		keychainStorage.delete(appTokenKey)
		keychainStorage.delete(appRefreshToken)
	}
}
