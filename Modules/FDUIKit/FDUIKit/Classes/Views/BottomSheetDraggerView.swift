//
//  BottomSheetDraggerView.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 19.12.2020.
//  Copyright © 2020 chocolife.me. All rights reserved.
//

import UIKit

final public class BottomSheetDraggerView: UIView {
    
	public let draggerView = UIViewFactory(backgroundColor: .silver)
        .circleCorners()
        .build()
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .clear
        addSubviews()
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        draggerView.roundCorners()
    }
}

// MARK: - Decoratable
extension BottomSheetDraggerView: Decoratable {
	public func addSubviews() {
        addSubviews(draggerView)
    }
    
	public func setupLayout() {
        draggerView.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.width.equalTo(Constants.draggerViewWidth)
            $0.height.equalTo(Constants.draggerViewHeight)
        }
    }
}

private enum Constants {
	public static let draggerViewWidth = 56
	public static let draggerViewHeight = 4
}
