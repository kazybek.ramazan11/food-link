//
//  PrimaryComponentButton.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 27.08.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit

fileprivate let circleRadius: CGFloat = 15

public enum PrimaryButtonStyle {
    case primary
    case secondary
}

public class PrimaryComponentButton: UIButton {
    
	public var style: PrimaryButtonStyle = .primary {
        didSet {
            setButtonAppearance()
        }
    }
    
    private var savedTitle: String? = nil
    private var savedCornerRadius: CGFloat = 0
    private var isAnimating = false
    
	public lazy var spinner: SpinnerView = {
        let spinner = SpinnerView()
        return spinner
    }()
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        setButtonAppearance()
        
        self.addSubviews([spinner])
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setButtonAppearance() {
        self.setTitleColor(.white, for: .normal)
        if self.style == .primary {
            self.backgroundColor = .appGreen
            self.layer.borderWidth = 0
        } else {
            self.backgroundColor = .clear
            self.layer.borderWidth = 1
        }
        self.layer.borderColor = UIColor.silver.cgColor
        self.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        self.layer.cornerRadius = 8
        
        self.enable()
        self.layoutIfNeeded()
    }
    
    public func startLoading() {
        self.superview?.endEditing(true)
        self.superview?.layoutIfNeeded()
        
        spinner.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset((self.frame.height - (circleRadius * 2)) / 2)
            make.width.height.equalTo(circleRadius * 2)
            make.centerY.equalToSuperview()
        }
        
        spinner.animate()
        startAnimation()
    }
    
    public func disable() {
        backgroundColor = .borderGrey
        setTitleColor(UIColor.white.withAlphaComponent(0.5), for: .normal)
        isUserInteractionEnabled = false
    }
    
    public func enable() {
        backgroundColor = .appGreen
        setTitleColor(.appBlack, for: .normal)
        isUserInteractionEnabled = true
    }
    
    private func startAnimation() {
        isAnimating = true
        self.isUserInteractionEnabled = false
        self.savedTitle = title(for: .normal)
        self.savedCornerRadius = layer.cornerRadius
        
        self.setTitle("",  for: .normal)
        
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.layer.cornerRadius = self.frame.height / 2
        }, completion: { completed -> Void in
            self.minimizeWidth()
            self.spinner.animate()
        })
    }
    
    private func minimizeWidth() {
        let widthAnimation = CABasicAnimation(keyPath: "bounds.size.width")
        widthAnimation.fromValue = frame.width
        widthAnimation.toValue = frame.height
        widthAnimation.duration = 0.1
        widthAnimation.timingFunction = CAMediaTimingFunction(name: .linear)
        widthAnimation.fillMode = .forwards
        widthAnimation.isRemovedOnCompletion = false
        
        layer.add(widthAnimation, forKey: widthAnimation.keyPath)
    }
    
    public func stopLoading(completion: (() -> ())? = nil) {
        guard isAnimating else { return }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.isAnimating = false
            self.setOriginalState(completion: completion)
        }
    }
    
    private func setOriginalState(completion: (() -> ())? = nil) {
        self.animateToOriginalWidth(completion: completion)
        self.spinner.stopAnimation()
        self.setTitle(self.savedTitle, for: .normal)
        self.isUserInteractionEnabled = true
        self.layer.cornerRadius = self.savedCornerRadius
    }
    
    private func animateToOriginalWidth(completion: (() -> ())? = nil) {
        let widthAnimation = CABasicAnimation(keyPath: "bounds.size.width")
        widthAnimation.fromValue = (self.bounds.height)
        widthAnimation.toValue = (self.bounds.width)
        widthAnimation.duration = 0.1
        widthAnimation.timingFunction = CAMediaTimingFunction(name: .linear)
        widthAnimation.fillMode = .forwards
        widthAnimation.isRemovedOnCompletion = false
        
        CATransaction.setCompletionBlock {
            completion?()
            self.layer.removeAllAnimations()
        }
        self.layer.add(widthAnimation, forKey: widthAnimation.keyPath)
        
        CATransaction.commit()
    }
    
}

public class SpinnerView : UIView {
    
	public let circlePathLayer = CAShapeLayer()
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        configure()
        circlePathLayer.isHidden = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        circlePathLayer.frame = bounds
        circlePathLayer.path = circlePath().cgPath
    }
    
	public func configure() {
        circlePathLayer.frame = bounds
        circlePathLayer.lineWidth = 2
        circlePathLayer.fillColor = UIColor.clear.cgColor
        circlePathLayer.strokeColor = UIColor.appBlack.withAlphaComponent(0.6).cgColor
        layer.addSublayer(circlePathLayer)
        backgroundColor = .clear
    }
    
	public func circlePath() -> UIBezierPath {
        self.layoutIfNeeded()
        let startAngle = 0 - Double.pi/2
        let endAngle = Double.pi * 2 - Double.pi
        return UIBezierPath(arcCenter: CGPoint(x: circleRadius, y: circleRadius), radius: circleRadius, startAngle: CGFloat(startAngle), endAngle: CGFloat(endAngle), clockwise: true)
    }
    
	public func animate() {
        self.circlePathLayer.isHidden = false
        let rotate = CABasicAnimation(keyPath: "transform.rotation.z")
        rotate.fromValue = 0
        rotate.toValue = Double.pi * 2
        rotate.duration = 0.4
        rotate.timingFunction = CAMediaTimingFunction(name: .linear)
        
        rotate.repeatCount = HUGE
        rotate.fillMode = .forwards
        rotate.isRemovedOnCompletion = false
        self.circlePathLayer.add(rotate, forKey: rotate.keyPath)
        
    }
    
	public func stopAnimation() {
        self.circlePathLayer.removeAllAnimations()
        self.circlePathLayer.isHidden = true
        
    }
    
}
