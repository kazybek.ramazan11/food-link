//
//  ErrorShowableTextFieldView.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 16.07.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit

public class ErrorShowableTextFieldView: UIView {
    
    // MARK: - Properties
    public var onToggleError: (() -> Void)?
    private var padding: UIEdgeInsets
    private let animationDuration: Double = 2
    
    // MARK: - Views
    private lazy var errorLabel: UILabel =
        UILabelFactory(text: "")
            .numberOf(lines: 0)
            .text(color: .interfaceRed)
            .isHidden(true)
            .font(FontFamily.Roboto.regular.font(size: 12))
            .build()
    
    private(set) lazy var textField: UITextField =
        UITextFieldFactory(placeholder: "", padding: self.padding)
            .corner(radius: 6)
            .tint(color: .appBlack)
            .font(FontFamily.Roboto.regular.font(size: 14))
            .text(color: .appBlack)
            .build()
    
    // MARK: - Inits
    public init(placeholder: String, padding: UIEdgeInsets = .zero) {
        self.padding = padding
        super.init(frame: .zero)
        
        addSubviews()
        setupLayout()
        setPlaceholder(placeholder: placeholder)
    }

    required init?(coder aDecoder: NSCoder) {
        self.padding = .zero
        super.init(coder: aDecoder)
        
        addSubviews()
        setupLayout()
    }
}

// MARK: - Methods
extension ErrorShowableTextFieldView {
    public func setPlaceholder(placeholder: String) {
        textField.attributedPlaceholder = NSAttributedString(string: placeholder,
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
    }
    
    public func setErrorText(_ text: String) {
        errorLabel.text = text
    }
    
    public func showError(_ text: String) {
        errorLabel.text = text
        errorLabel.isHidden = false
        textField.layer.borderColor = UIColor.interfaceRed.cgColor
        errorLabel.textColor = .interfaceRed
        UIView.animate(withDuration: animationDuration) {
            self.errorLabel.snp.updateConstraints { (make) in
                make.top.equalTo(self.textField.snp.bottom).offset(4)
            }
            self.textField.snp.remakeConstraints { (make) in
                make.top.equalToSuperview()
                make.left.right.equalTo(self.errorLabel)
                make.height.equalTo(40)
            }
        }
        layoutIfNeeded()
    }
    
    public func dismissError() {
        UIView.animate(withDuration: animationDuration) {
            self.errorLabel.snp.updateConstraints { (make) in
                make.top.equalTo(self.textField.snp.bottom)
            }
            self.textField.snp.remakeConstraints { (make) in
                make.top.bottom.equalToSuperview()
                make.left.right.equalTo(self.errorLabel)
                make.height.equalTo(40)
            }
        }
        errorLabel.isHidden = true
        errorLabel.textColor = .black
        textField.layer.borderColor = UIColor.borderGrey.cgColor
    }
    
    public func clear() {
        textField.text = ""
        textField.attributedText = NSAttributedString(string: "")
    }
    
    public func resign() {
        textField.resignFirstResponder()
    }
}


// MARK: - Decoratable
extension ErrorShowableTextFieldView: Decoratable {
	public func addSubviews() {
        [errorLabel, textField].forEach { addSubview($0) }
    }
    
	public func setupLayout() {
        errorLabel.snp.makeConstraints { (make) in
            make.top.equalTo(textField.snp.bottom)
            make.left.bottom.right.equalToSuperview()
        }
        textField.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.right.equalTo(errorLabel)
            make.height.equalTo(40)
        }
    }
}
