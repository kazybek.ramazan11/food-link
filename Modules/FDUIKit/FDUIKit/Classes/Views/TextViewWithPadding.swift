//
//  TextViewWithPadding.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 19.07.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit
class TextViewWithPadding: UITextView {
    
    private var padding: UIEdgeInsets
    
    public init(padding: UIEdgeInsets = UIEdgeInsets(top: 15, left: 15, bottom: 16, right: 16)) {
        self.padding = padding
        super.init(frame: CGRect.zero, textContainer: nil)
        textContainerInset = padding
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
