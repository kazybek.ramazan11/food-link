//
//  AvatarImageView.swift
//  FDUIKit
//
//  Created by Ramazan Kazybek on 5/8/21.
//

import Kingfisher

public protocol AvatarImageViewDelegate: AnyObject {
	func addButtonDidTap()
}

final public class AvatarImageView: UIView {
	public weak var delegate: AvatarImageViewDelegate?
	
	private let containerView = UIView()
	
	private let imageButton: UIButton = {
		let button = UIButton(type: .custom)
		button.layer.borderWidth = 2
		button.layer.borderColor = UIColor.white.cgColor
		button.clipsToBounds = true
		button.imageView?.contentMode = .scaleAspectFit
		button.imageEdgeInsets = .init(top: 9, left: 9, bottom: 9, right: 9)
		button.backgroundColor = .white
		button.isEnabled = false
		return button
	}()
	
	private lazy var addButton: UIButton = {
		let button = UIButton()
		button.setImage(.init(#imageLiteral(resourceName: "add_button")), for: .normal)
		button.addTarget(self, action: #selector(addButtonTapped), for: .touchUpInside)
		return button
	}()
	
	public override init(frame: CGRect) {
		super.init(frame: frame)
		imageButton.setImage(UIImage(#imageLiteral(resourceName: "profile_logo")), for: .normal)
		setupViews()
	}
	
	init(image: UIImage = UIImage(#imageLiteral(resourceName: "profile_logo"))) {
		super.init(frame: .zero)
		imageButton.setImage(image, for: .normal)
		setupViews()
	}
	
	public required init?(coder: NSCoder) {
		nil
	}
	
	public override func layoutSubviews() {
		super.layoutSubviews()
		imageButton.layer.cornerRadius = frame.width / 2
	}
	
	private func setupViews() {
		addSubview(imageButton)
		addSubview(addButton)
		imageButton.snp.makeConstraints { make in
			make.edges.equalToSuperview()
		}
				
		addButton.snp.makeConstraints { make in
			make.bottom.trailing.equalTo(imageButton)
		}
	}
	
	@objc
	private func addButtonTapped() {
		delegate?.addButtonDidTap()
	}
	
	public func setImage(_ image: UIImage) {
		imageButton.setImage(image, for: .normal)
	}
	
	public func setImage(_ url: URL) {
		imageButton.imageView?.kf.setImage(with: url)
	}
	
	public func border(color: UIColor) {
		imageButton.layer.borderColor = color.cgColor
	}
}
