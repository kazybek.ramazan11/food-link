//
//  TextFieldWithPadding.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 17.07.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit
import JMMaskTextField_Swift

public class TextFieldWithPadding: JMMaskTextField {
    private var padding: UIEdgeInsets
    
    public init(padding: UIEdgeInsets = UIEdgeInsets(top: 15, left: 15, bottom: 16, right: 16)) {
        self.padding = padding
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override public func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override public func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
