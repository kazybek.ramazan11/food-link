//
//  UIViewFactory.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 03.08.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit

public final class UIViewFactory {
    private var view: UIView

    public enum Style {
        case normal
    }
    // MARK: - Inits

	public init(backgroundColor: UIColor, style: Style = .normal) {
        self.view = UIView()
        view.backgroundColor = backgroundColor
        switch style {
        case .normal:
            normalStyle()
        }
    }
    
    convenience public  init(style: Style = .normal) {
        self.init(backgroundColor: .clear)
    }

    // MARK: - Styles
    public func normalStyle() {
        
    }

    // MARK: - Public methods
    public func corner(radius: CGFloat) -> Self {
        view.layer.cornerRadius = radius
        
        return self
    }
    
    public func circleCorners() -> Self {
        view.roundCorners()
        
        return self
    }
    
    public func addBorder(width: CGFloat, color: CGColor) -> Self {
        view.layer.borderColor = color
        view.layer.borderWidth = width
        
        return self
    }

    public func border(width: CGFloat = 0, color: UIColor = .clear) -> Self {
        view.layer.borderWidth = width
        view.layer.borderColor = color.cgColor
        
        return self
    }

    public func tint(color: UIColor) -> Self {
        view.tintColor = color
        
        return self
    }
    
    public func background(color: UIColor) -> Self {
        view.backgroundColor = color
        
        return self
    }
    
    public func isHidden(_ flag: Bool) -> Self {
        view.isHidden = flag
        
        return self
    }

    public func build() -> UIView {
        return view
    }
}
