//
//  UIImageViewFactory.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 03.11.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit
import Kingfisher

public final class UIImageViewFactory {
    private let _imageView: UIImageView
    
	public init(image: UIImage?) {
        _imageView = UIImageView(image: image)
        _imageView.contentMode = .scaleAspectFit
    }
    
	public init(image: UIImage?, renderingMode: UIImage.RenderingMode) {
        _imageView = UIImageView(image: image?.withRenderingMode(renderingMode))
        _imageView.contentMode = .scaleAspectFit
    }
    
	public init() {
        _imageView = UIImageView()
        _imageView.kf.indicatorType = .activity
        _imageView.contentMode = .scaleAspectFit
    }
    
    public func content(mode: UIView.ContentMode) -> Self {
        _imageView.contentMode = mode
        
        return self
    }
    
    public func roundCorners() -> Self {
        _imageView.roundCorners()
        
        return self
    }
    
    public func corner(radius: CGFloat) -> Self {
        _imageView.corner(radius: radius)
        
        return self
    }
    
    public func clipsToBounds(_ flag: Bool) -> Self {
        _imageView.clipsToBounds = flag
        
        return self
    }
    
    public func background(color: UIColor) -> Self {
        _imageView.backgroundColor = color
        
        return self
    }
    
    public func tint(color: UIColor) -> Self {
        _imageView.tintColor = color
        
        return self
    }
    
    public func isUserInteraction(enabled: Bool) -> Self {
        _imageView.isUserInteractionEnabled = enabled
        
        return self
    }
    
    public func tag(_ value: Int) -> Self {
        _imageView.tag = value
        
        return self
    }
    
    public func build() -> UIImageView {
        return _imageView
    }
}
