//
//  UIButtonFactory.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 17.07.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit

public final class UIButtonFactory {
    private var button: UIButton

	public enum Style {
        case normal
        case underlined
        case primary
     }

    // MARK: - Inits
	public init(type: UIButton.ButtonType = .custom, title: String = "", style: Style = .normal, color: UIColor = .white, background: UIColor = .appBlue, cornerRadius: CGFloat = 4) {
        
        button = UIButton(type: type)
		button.setTitle(title, for: .normal)
        switch style {
        case .normal:
            normalStyle()
        case .underlined:
            underlinedStyle()
        case .primary:
            button = PrimaryComponentButton()
        }
        button.clipsToBounds = true
        button.setTitleColor(color, for: .normal)
        button.layer.cornerRadius = cornerRadius
    }
    
	public init(type: UIButton.ButtonType = .custom, image: UIImage, imageState: UIControl.State = .normal) {
        button = UIButton(type: type)
        button.setImage(image, for: imageState)
        button.clipsToBounds = true
    }

    // MARK: - Styles
	public func normalStyle() {
        button.titleLabel?.font = FontFamily.Roboto.regular.font(size: 14)
    }

	public func underlinedStyle() {
        guard let text = button.titleLabel?.text else { return }
        let attributedString = NSMutableAttributedString(string: text)
        //NSAttributedStringKey.foregroundColor : UIColor.blue
        attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: button.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: button.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
        button.setAttributedTitle(attributedString, for: .normal)
    }

  // MARK: - Public methods
	public func font(size: CGFloat) -> Self {
        button.titleLabel?.font = button.titleLabel?.font.withSize(size)

        return self
    }
    
	public func font(_ font: UIFont) -> Self {
        button.titleLabel?.font = font

        return self
    }

	public func title(color: UIColor) -> Self {
        button.setTitleColor(color, for: .normal)

        return self
    }
	
	public func tint(color: UIColor) -> Self {
		button.tintColor = color

		return self
	}

	public func background(color: UIColor) -> Self {
        button.backgroundColor = color

        return self
    }
    
	public func set(image: UIImage, for state: UIControl.State = .normal, imageEdgeInsets: UIEdgeInsets = .zero) -> Self {
        button.setImage(image, for: state)
        button.imageView?.isUserInteractionEnabled = false
        button.imageEdgeInsets = imageEdgeInsets
        
        return self
    }
    
	public func addTarget(_ target: Any?, action: Selector, for event: UIControl.Event = .touchUpInside) -> Self {
        button.addTarget(target, action: action, for: event)
        
        return self
    }
    
	public func add(imageEdgeInsets: UIEdgeInsets = .zero) -> Self {
        button.imageEdgeInsets = imageEdgeInsets
        
        return self
    }

	public func corner(radius: CGFloat) -> Self {
        button.layer.cornerRadius = radius

        return self
    }
    
	public func content(mode: UIView.ContentMode) -> Self {
        button.contentMode = mode
        
        return self
    }
    
	public func isHidden(_ hidden: Bool) -> Self {
        button.isHidden = hidden
        
        return self
    }

	public func build() -> UIButton {
        return button
    }
	
	public func underline() -> UIButton {
		return button
	}
}
