//
//  UIViewController+Snackbar.swift
//  FDUIKit
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import Foundation
import SwiftMessages
import UIKit

private enum Constants {
	static let defaultSnackbarDuration: TimeInterval = 2
}

public extension UIViewController {
	func showSuccess(message: String?) {
		showSnackbar(type: .success(title: message))
	}
	
	func showError(message: String?) {
		showSnackbar(type: .error(title: message))
	}
	
	func showSnackbar(type: SnackbarType, duration: SwiftMessages.Duration = .seconds(seconds: 2)) {
		switch type {
		case let .error(title), let .success(title):
			showStatusSnackbar(duration: duration, title: title, type: type)
		case let .info(title, action, actionTitle):
			showInfoSnackbar(duration: duration, title: title, action: action, actionTitle: actionTitle)
		case let .loading(title, progressTitle):
			showLoadingSnackBar(duration: .forever, title: title, progressTitle: progressTitle)
		}
	}
	
	private func showInfoSnackbar(duration: SwiftMessages.Duration, title: String?, action: @escaping () -> Void, actionTitle: String) {
		var config = SnackbarConfig.shared.config
		config.duration = duration
		guard let view: InfoSnackbarView = try? SwiftMessages.viewFromNib(
				named: String(describing: InfoSnackbarView.self),
				bundle: .main) else { return }
		view.onButtonTap = action
		view.configure(title: title, actionTitle: actionTitle)
		SwiftMessages.show(config: config, view: view)
	}
	
	private func showStatusSnackbar(duration: SwiftMessages.Duration, title: String?, type: SnackbarType) {
		guard let view: StatusSnackbarView = try? SwiftMessages.viewFromNib(
				named: String(describing: StatusSnackbarView.self),
				bundle: .main) else { return }
		var config = SnackbarConfig.shared.config
		config.duration = duration
		view.configure(title: title, type: type)
		SwiftMessages.show(config: config, view: view)
	}
	
	private func showLoadingSnackBar(duration: SwiftMessages.Duration, title: String?, progressTitle: @escaping () -> String) {
		guard let view: LoadingSnackbarView = try? SwiftMessages.viewFromNib(
				named: String(describing: LoadingSnackbarView.self),
				bundle: .main) else { return }
		var config = SnackbarConfig.shared.config
		config.duration = duration
		view.configure(title: title, progressTitle: progressTitle)
		SwiftMessages.show(config: config, view: view)
	}
}
