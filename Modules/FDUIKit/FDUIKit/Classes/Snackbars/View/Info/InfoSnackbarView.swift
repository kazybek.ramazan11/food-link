//
//  InfoSnackbarView.swift
//  FDUIKit
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import SwiftMessages

final public class InfoSnackbarView: MessageView {
	var onButtonTap: (() -> Void)?
	
	public override func awakeFromNib() {
		super.awakeFromNib()
		configureView()
	}

	func configure(title: String?, actionTitle: String) {
		titleLabel?.text = title
		button?.setTitle(actionTitle, for: .normal)
	}
	
	private func configureView() {
		button!.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
	}
	
	@objc
	private func buttonAction() {
		onButtonTap?()
	}
}
