//
//  StatusSnackbarView.swift
//  FDUIKit
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import SwiftMessages

final public class StatusSnackbarView: MessageView {
	var type: SnackbarType = .success(title: "") {
		didSet {
			backgroundView.backgroundColor = type.backgroundColor
			iconImageView?.image = type.statusImage
		}
	}
	
	func configure(title: String?, type: SnackbarType) {
		self.type = type
		titleLabel?.text = title
	}
}
