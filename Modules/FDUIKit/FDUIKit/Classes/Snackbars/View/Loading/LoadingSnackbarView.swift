//
//  LoadingSnackbarView.swift
//  FDUIKit
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import SwiftMessages

final class LoadingSnackbarView: MessageView {
	@IBOutlet private var activityIndicator: UIActivityIndicatorView!

	override func awakeFromNib() {
		super.awakeFromNib()
		configureView()
	}
	
	func configure(title: String?, progressTitle: @escaping () -> String) {
		titleLabel?.text = title
		bodyLabel?.text = progressTitle()
	}
	
	private func configureView() {
		activityIndicator.startAnimating()
	}
}
