//
//  SnackbarType.swift
//  FDUIKit
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import UIKit

public enum SnackbarType {
	case info(title: String?, action: () -> Void, actionTitle: String)
	case loading(title: String?, progressTitle: () -> String)
	case error(title: String?)
	case success(title: String?)
	
	var backgroundColor: UIColor {
		switch self {
		case .error:
			return .interfaceRed
		case .success:
			return .appOrange
		default:
			return .appGray
		}
	}
	
	var statusImage: UIImage? {
		switch self {
		case .success:
			return nil
//			return R.image.check()
		case .error:
			return nil
//			return R.image.wrong()
		default:
			return nil
		}
	}
}
