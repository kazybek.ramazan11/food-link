//
//  SnackbarConfig.swift
//  FDUIKit
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import SwiftMessages

final class SnackbarConfig: SwiftMessages {
	static let shared = SnackbarConfig()

	lazy var config: Config = {
		var config = Config()
		config.becomeKeyWindow = true
		config.presentationContext = .window(windowLevel: .alert)
		config.presentationStyle = .bottom
		config.keyboardTrackingView = KeyboardTrackingView()
		config.interactiveHide = true
		return config
	}()
}
