//
//  Colors.swift
//  food_link
//
//  Created by Ramazan Kazybek on 3/14/21.
//

import UIKit

public extension UIColor {
	
	// MARK: - Properties
	static let appBlue = UIColor(hex: "#4688D8")
	static let appGray = UIColor(hex: "#8E8A8A")
	static let appGreen = UIColor(hex: "#00C667")
	static let appOrange = UIColor(hex: "#FF7C60")
	static let secondaryBlue = UIColor(hex: "#E0EBF8")
	static let additionalGrey = UIColor(hex: "#A6A6A6")
	static let interfaceGray = UIColor(hex: "#979797")
	static let linkBlue = UIColor(hex: "#2877D8")
	static let appBlack = UIColor(hex: "#303030")
	static let darkGray = UIColor(hex: "#C0C0C0")
	static let interfaceRed = UIColor(hex: "#EB5757")
	static let fieldBackgroundGray = UIColor(hex: "#E9E8E8")
	static let borderGrey = UIColor(hex: "#E1E1E1")
	static let lightGrey = UIColor(hex: "#EDEDED")
	static let wildSand = UIColor(hex: "#E5E4E2")
	static let backgroundGrey = UIColor(hex: "#E5E5E5")
	static let handleGrey = UIColor(hex: "#C4C4C4")
	static let lightningYellow = UIColor(hex: "#FCCC22")
	static let burntSienna = UIColor(hex: "#E64F46")
	static let successGreen = UIColor(hex: "#27AE60")
	static let doveGray = UIColor(hex: "#707070")
	static let silver = UIColor(hex: "#BDC3C7")
	static let alto = UIColor(hex: "#E0E0E0")
	static let avatarBackgroundGray = UIColor(hex: "#F5F5F5")
	static let bubbleBackgroundGray = UIColor(hex: "#F4F4F4")
	static let tableSeparatorGray = UIColor(hex: "#F2F2F2")
	static let callAdditionalGray = UIColor(hex: "#666666")
	static let pickerYellow = UIColor(hex: "#FCCC22")
	static let audioSliderBlue = UIColor(hex: "#5FA7FF")
	static let filterBlue = UIColor(hex: "#DAE7F7")
	static let activeGreen = UIColor(hex: "#44DE2B")
	static let placeholderGray = UIColor(hex: "#7B7575")
	static let disabledGray = UIColor(hex: "#DADADA")
	static let lowOpacityBlack = UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0.64)
	static let filterBackgroundColor = UIColor(r: 255, g: 255, b: 255, a: 0.1)
	static let appBackground = UIColor(hex: "#F6F8F7")
	static let whiteShadowColor = UIColor(r: 0, g: 0, b: 0, a: 0.1)
}
