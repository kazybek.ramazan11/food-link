//
//  UIView+Extensions.swift
//  food_link
//
//  Created by Ramazan Kazybek on 3/14/21.
//


import Foundation
import UIKit
import SnapKit

public extension UIView {
	public func corner(radius: CGFloat, corners: UIRectCorner) {
		let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
		let mask = CAShapeLayer()
		mask.path = path.cgPath
		self.layer.mask = mask
	}
	
	public func corner(radius: CGFloat) {
		layer.cornerRadius = radius
		layer.masksToBounds = true
	}
	
	public func roundCorners() {
		corner(radius: frame.height / 2)
	}
	
	public func fillSuperview() {
		snp.makeConstraints { (make) in
			make.edges.equalToSuperview()
		}
	}
	
	public func addSubviews(_ views: [UIView]) {
		views.forEach({ addSubview( $0 ) })
	}
	
	public func addSubviews(_ views: UIView...) {
		views.forEach { addSubview($0) }
	}
	
	public static var name: String {
		return String(describing: self)
	}
	
	public class var identifier: String {
		return String(describing: self)
	}
	
	public func addShadow(ofColor color: UIColor = UIColor(red: 0.07, green: 0.47, blue: 0.57, alpha: 1.0), radius: CGFloat = 3, offset: CGSize = .zero, opacity: Float = 0.5) {
		layer.shadowColor = color.cgColor
		layer.shadowOffset = offset
		layer.shadowRadius = radius
		layer.shadowOpacity = opacity
		layer.masksToBounds = false
	}
	
	public func setAnchorPoint(_ point: CGPoint) {
		var newPoint = CGPoint(x: bounds.size.width * point.x, y: bounds.size.height * point.y)
		var oldPoint = CGPoint(x: bounds.size.width * layer.anchorPoint.x, y: bounds.size.height * layer.anchorPoint.y);
		
		newPoint = newPoint.applying(transform)
		oldPoint = oldPoint.applying(transform)
		
		var position = layer.position
		
		position.x -= oldPoint.x
		position.x += newPoint.x
		
		position.y -= oldPoint.y
		position.y += newPoint.y
		
		layer.position = position
		layer.anchorPoint = point
	}
	
	public func pushTransition(_ duration: CFTimeInterval) {
		let animation = CATransition()
		animation.timingFunction = CAMediaTimingFunction(name:
			CAMediaTimingFunctionName.easeInEaseOut)
		animation.type = CATransitionType.push
		animation.subtype = CATransitionSubtype.fromTop
		animation.duration = duration
		
		layer.add(animation, forKey: CATransitionType.push.rawValue)
	}
	
	public func popTransition(_ duration: CFTimeInterval) {
		let animation = CATransition()
		animation.timingFunction = CAMediaTimingFunction(name:
			CAMediaTimingFunctionName.easeInEaseOut)
		animation.type = CATransitionType.push
		animation.subtype = CATransitionSubtype.fromBottom
		animation.duration = duration
		
		layer.add(animation, forKey: CATransitionType.push.rawValue)
	}
}

//TODO: - remove after interface moved to code
//MARK:- IBInspectable
public extension UIView {
	var ibcornerRadius: CGFloat {
		get {
			return layer.cornerRadius
		}
		set {
			layer.cornerRadius = newValue
			layer.masksToBounds = newValue > 0
		}
	}
	
	var ibborderWidth: CGFloat {
		get {
			return layer.borderWidth
		}
		set {
			layer.borderWidth = newValue
		}
	}
	
	var ibborderColor: UIColor? {
		get {
			return UIColor(cgColor: layer.borderColor!)
		}
		set {
			layer.borderColor = newValue?.cgColor
		}
	}
	
	var ibshadowRadius: CGFloat {
		get {
			return layer.shadowRadius
		}
		set {
			layer.masksToBounds = false
			layer.shadowRadius = newValue
		}
	}
	
	var ibshadowOpacity: Float {
		get {
			return layer.shadowOpacity
		}
		set {
			layer.masksToBounds = false
			layer.shadowOpacity = newValue
		}
	}
	
	var ibshadowOffset: CGSize {
		get {
			return layer.shadowOffset
		}
		set {
			layer.masksToBounds = false
			layer.shadowOffset = newValue
		}
	}
	
	var ibshadowColor: UIColor? {
		get {
			if let color = layer.shadowColor {
				return UIColor(cgColor: color)
			}
			return nil
		}
		set {
			if let color = newValue {
				layer.shadowColor = color.cgColor
			} else {
				layer.shadowColor = nil
			}
		}
	}
}
