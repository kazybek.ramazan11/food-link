//
//  Router+Transition.swift
//  KZSBUIKit
//
//  Created by Islam Temirbek on 11/25/20.
//


extension Router {
	public func presentBottomSheet(_ module: Presentable, animated: Bool = true) {
		// TODO: - Добавить transition delegate
//		controller.transitioningDelegate = BottomSheetModalTransitioningDelegate.default
		present(module, animated: animated, modalPresentationStyle: .custom)
	}
}
