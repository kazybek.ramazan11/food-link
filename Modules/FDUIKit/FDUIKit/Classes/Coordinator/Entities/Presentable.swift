//
//  Presentable.swift
//  Presentable
//
//  Created by Ramazan Kazybek on 11/24/20.
//

import UIKit

public protocol Presentable {
	func toPresent() -> UIViewController?
}

extension UIViewController: Presentable {
	public func toPresent() -> UIViewController? {
		self
	}
}
