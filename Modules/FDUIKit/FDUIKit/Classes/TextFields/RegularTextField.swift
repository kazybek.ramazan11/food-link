//
//  RegularTextField.swift
//  FDUIKit
//
//  Created by Ramazan Kazybek on 5/8/21.
//

import UIKit

private enum Constants {
	static let borderWidth: CGFloat = 0
}

open class RegularTextField: UITextField {
	
	public var currentState: RegularTextFieldState  = .normal {
		didSet {
			backgroundColor = currentState.backgroundColor
			bottomBorderLineView.backgroundColor = currentState.borderColor
			textColor = currentState.textColor
			font = currentState.textFont
		}
	}
	
	public let bottomBorderLineView = UIView()
	
	let view = UIView(frame: .init(origin: .zero, size: .init(width: 37, height: 20)))
	
	let imageView = UIImageView(frame: .init(origin: .zero, size: .init(width: 37, height: 20)))
	
	override public var placeholder: String? {
		didSet {
			configurePlaceholder()
		}
	}
	
	private let placeholderColor: UIColor = .appGray
	private let placeholderFont: UIFont = FontFamily.Roboto.regular.font(size: 14)
	
	override public var isEnabled: Bool {
		didSet {
			if !isEnabled {
				currentState = .disabled
			} else {
				currentState = .normal
			}
		}
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		configureView()
	}
	
	required public init?(coder: NSCoder) {
		super.init(coder: coder)
		configureView()
	}
	
	override public func textRect(forBounds bounds: CGRect) -> CGRect {
		let superRect = super.textRect(forBounds: bounds)
		return CGRect(
			x: superRect.origin.x + 18,
			y: superRect.origin.y,
			width: superRect.width - 16,
			height: superRect.height
		)
	}
	
	override public func editingRect(forBounds bounds: CGRect) -> CGRect {
		let superRect = super.editingRect(forBounds: bounds)
		return CGRect(
			x: superRect.origin.x + 18,
			y: superRect.origin.y,
			width: superRect.width - 16,
			height: superRect.height
		)
	}
	
	public func setLeftImage(image: UIImage?, tintColor: UIColor = .appOrange) {
		if leftViewMode != .always {
			leftViewMode = .always
		}
		if !view.subviews.contains(imageView) {
			view.addSubview(imageView)
		}
		imageView.image = image
		imageView.tintColor = tintColor
		leftView = view
	}
	
	private func configureView() {
		imageView.contentMode = .scaleAspectFit
		addSubview(bottomBorderLineView)
		layer.borderColor = UIColor.clear.cgColor
		autocorrectionType = .no
		configurePlaceholder()
		layer.borderWidth = Constants.borderWidth
		tintColor = .appOrange
		currentState = .normal
		setActions()
		bottomBorderLineView.snp.makeConstraints { make in
			make.height.equalTo(0.5)
			make.leading.trailing.bottom.equalToSuperview()
		}
	}
	
	private func configurePlaceholder() {
		attributedPlaceholder = NSAttributedString(string: placeholder ?? "",
												   attributes: [
													NSAttributedString.Key.foregroundColor: placeholderColor,
													NSAttributedString.Key.font: placeholderFont])
	}
	
	private func setActions() {
		addTarget(self, action: #selector(editingDidBegin), for: .editingDidBegin)
		addTarget(self, action: #selector(editingDidChanged), for: .editingChanged)
		addTarget(self, action: #selector(editingDidEnd), for: .editingDidEnd)
	}
	
	@objc
	func editingDidBegin() {
		currentState = .selected
	}
	
	@objc
	func editingDidChanged() {
		currentState = .selected
	}
	
	@objc
	func editingDidEnd() {
		currentState = .normal
	}
}
