//
//  RightButtonTextField.swift
//  FDUIKit
//
//  Created by Ramazan Kazybek on 5/8/21.
//

import UIKit

private enum Constants {
	static let buttonWidth: CGFloat = 22
	static let buttonHeight: CGFloat = 20
	static let rightSpacing: CGFloat = -17
}

open class RightButtonTextField: RegularTextField {

	public var buttonAction: (() -> Void)?
	private var image: UIImage?
	private var resignImage: UIImage?
	
	override public var isEnabled: Bool {
		didSet {
			currentState = .normal
		}
	}
	
	private let button: UIButton = {
		let button = UIButton()
		return button
	}()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		configureView()
	}
	
	required public init?(coder: NSCoder) {
		super.init(coder: coder)
		configureView()
	}
	
	
	open override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
		var textRect = super.leftViewRect(forBounds: bounds)
//		textRect.origin.x += 2
		return textRect
	}
	override public func textRect(forBounds bounds: CGRect) -> CGRect {
		let superRect = super.textRect(forBounds: bounds)
		return CGRect(
			x: superRect.origin.x,
			y: 0,
			width: superRect.width - Constants.buttonWidth + Constants.rightSpacing - 16,
			height: superRect.height
		)
	}
	
	override public func editingRect(forBounds bounds: CGRect) -> CGRect {
		let superRect = super.editingRect(forBounds: bounds)
		return CGRect(
			x: superRect.origin.x,
			y: 0,
			width: superRect.width - Constants.buttonWidth + Constants.rightSpacing - 16,
			height: superRect.height
		)
	}

	private func setButtonImage(image: UIImage?) {
		button.setImage(image, for: .normal)
		button.tintColor = tintColor
	}
	
	public func setButtonImage(image: UIImage?, resignImage: UIImage? = nil) {
		self.image = image
		self.resignImage = resignImage ?? image
		setButtonImage(image: resignImage ?? image)
	}
	
	@objc
	private func buttonTouchHandler() {
		if currentState == .selected {
			setButtonImage(image: resignImage)
			resignFirstResponder()
			buttonAction?()
			return
		}
		if canBecomeFirstResponder {
			becomeFirstResponder()
		}
		setButtonImage(image: image)
	}
	
	private func configureView() {
		setupInitialLayout()
		button.addTarget(self, action: #selector(buttonTouchHandler), for: .touchUpInside)
		button.imageView?.contentMode = .scaleAspectFit
	}

	private func setupInitialLayout() {
		addSubview(button)
		button.trailingAnchor.constraint(equalTo: trailingAnchor, constant: Constants.rightSpacing).isActive = true
		button.snp.makeConstraints { make in
			make.centerY.equalToSuperview()
			make.width.equalTo(Constants.buttonWidth)
			make.height.equalTo(Constants.buttonHeight)
		}
	}
	
	override func editingDidBegin() {
		super.editingDidBegin()
		setButtonImage(image: image)
	}
	
	override func editingDidEnd() {
		super.editingDidEnd()
		setButtonImage(image: resignImage)
		buttonAction?()
	}
}
