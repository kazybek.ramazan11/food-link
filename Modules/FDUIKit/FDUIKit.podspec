#
# Be sure to run `pod lib lint FDUIKit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'FDUIKit'
  s.version          = '0.1.0'
  s.summary          = 'A short description of FDUIKit.'
  s.homepage         = 'https://github.com/iBol/FDUIKit'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'iBol' => 'aibolseed@gmail.com' }
  s.source           = { :git => 'https://github.com/iBol/FDUIKit.git', :tag => s.version.to_s }
  s.ios.deployment_target = '12.0'
  s.source_files = 'FDUIKit/Classes/**/*'
	s.dependency 'SkyFloatingLabelTextField'
	s.dependency 'Kingfisher'
	s.dependency 'SkeletonView'
	s.dependency 'SnapKit'
	s.dependency 'JMMaskTextField-Swift'
	s.resource_bundles = { 'FDUIKit-Assets' => ['FDUIKit/Assets/**/*'] }
	s.dependency 'SwiftMessages'
  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
