# FDUIKit

[![CI Status](https://img.shields.io/travis/iBol/FDUIKit.svg?style=flat)](https://travis-ci.org/iBol/FDUIKit)
[![Version](https://img.shields.io/cocoapods/v/FDUIKit.svg?style=flat)](https://cocoapods.org/pods/FDUIKit)
[![License](https://img.shields.io/cocoapods/l/FDUIKit.svg?style=flat)](https://cocoapods.org/pods/FDUIKit)
[![Platform](https://img.shields.io/cocoapods/p/FDUIKit.svg?style=flat)](https://cocoapods.org/pods/FDUIKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FDUIKit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FDUIKit'
```

## Author

iBol, aibolseed@gmail.com

## License

FDUIKit is available under the MIT license. See the LICENSE file for more info.
