# FDStock

[![CI Status](https://img.shields.io/travis/iBol/FDStock.svg?style=flat)](https://travis-ci.org/iBol/FDStock)
[![Version](https://img.shields.io/cocoapods/v/FDStock.svg?style=flat)](https://cocoapods.org/pods/FDStock)
[![License](https://img.shields.io/cocoapods/l/FDStock.svg?style=flat)](https://cocoapods.org/pods/FDStock)
[![Platform](https://img.shields.io/cocoapods/p/FDStock.svg?style=flat)](https://cocoapods.org/pods/FDStock)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FDStock is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FDStock'
```

## Author

iBol, aibolseed@gmail.com

## License

FDStock is available under the MIT license. See the LICENSE file for more info.
