//
//  StockCoordinator.swift
//  FDOrders
//
//  Created by iBol on 5/10/21.
//

import FDUIKit
import EasyDi

public typealias IStockCoordinatorOutput = (IStockCoordinator & ExitCoordinator)

public protocol IStockCoordinator: AnyObject {}

public final class StockCoordinator: BaseCoordinator, IStockCoordinatorOutput {
	// MARK: - ExitCoordinator
	
	public var exitCompletion: Callback?
	
	// MARK: - Lifecycle
	
	public override init(router: Router) {
		super.init(router: router)
	}
	
	public override func start() {
		navigateToStock()
	}
	
	private func navigateToStock() {
		let assembly: StockAssembly = DIContext.defaultInstance.assembly()
		let controller = assembly.makeModule(with: self)
		router.push(controller, animated: true, hideBottomBarWhenPushed: false, completion: nil)
	}
}
