//
//  StockAssembly.swift
//  FDOrders
//
//  Created by iBol on 5/10/21.
//

import EasyDi

final class StockAssembly: Assembly {
	// MARK: - ViewController
	
	func makeModule(with coordinator: IStockCoordinatorOutput) -> UIViewController {
		let interactor = makeInteractor()
		let controller = StockViewController(interactor: interactor)
		
		return define(init: controller) {
			interactor.controller = $0
			interactor.coordinator = coordinator
			return $0
		}
	}
	
	// MARK: - Interactor
	
	private func makeInteractor() -> StockInteractor {
		return StockInteractor()
	}
}
