//
//  StockViewController.swift
//  FDOrders
//
//  Created by iBol on 5/10/21.
//

import UIKit
import SnapKit

protocol IStockView: AnyObject {}

final class StockViewController: UIViewController {
	// MARK: - Deps
	
	private let interactor: IStockInteractor
	
	// MARK: - Lifecycle
	
	init(interactor: IStockInteractor) {
		self.interactor = interactor
		
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		view.backgroundColor = .blue
	}
}

// MARK: - IStockView

extension StockViewController: IStockView {}
