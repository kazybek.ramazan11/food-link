//
//  StockInteractor.swift
//  FDOrders
//
//  Created by iBol on 5/10/21.
//

import UIKit

protocol IStockInteractor: AnyObject {}

final class StockInteractor: IStockInteractor {
	// MARK: - Deps
	
	weak var controller: IStockView?
	weak var coordinator: IStockCoordinatorOutput?
}
