//
//  ProfileViewController.swift
//  food_link
//
//  Created by Ramazan Kazybek on 5/7/21.
//

import SwiftUI
import UIKit
import FDUIKit

private enum Constants {
	static let gradientLayerHeight: CGFloat = 200
	static let buttonWidth: CGFloat = 56
	static let buttonCornerRadius: CGFloat = buttonWidth / 2
}

final class CreateProfileViewController: UIViewController {
	private let gradientLayer: CAGradientLayer = {
		let layer = CAGradientLayer()
		layer.colors = [UIColor.appOrange.cgColor, UIColor.appOrange.cgColor]
		layer.startPoint = CGPoint(x: 0, y: 0)
		layer.endPoint = CGPoint(x: 1, y: 1)
		return layer
	}()
	
	private let textFieldStackView = CreateProfileStackView()
	
	private let avatarView = AvatarImageView()
	
	private let nextButton: UIButton =
		UIButtonFactory(image: UIImage(imageLiteralResourceName: "long_right_chevron"))
		.background(color: .appOrange)
		.corner(radius: Constants.buttonCornerRadius)
		.build()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setupViews()
		setupConstraints()
	}
	
	private func setupNavBar() {
		navigationController?.navigationBar.barTintColor = UIColor.appOrange
		navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
		navigationController?.navigationBar.shadowImage = UIImage()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		setupNavBar()
	}
	
	override func viewWillLayoutSubviews() {
		gradientLayer.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.safeAreaInsets.top + Constants.gradientLayerHeight)
	}
	
	private func setupViews() {
		view.backgroundColor = .appBackground
		nextButton.addShadow(ofColor: .black, radius: 7, offset: .init(width: 0, height: 8), opacity: 0.4)
		[textFieldStackView, avatarView, nextButton].forEach { view.addSubview($0) }
		view.layer.insertSublayer(gradientLayer, at: 0)
	}
	
	private func setupConstraints() {
		textFieldStackView.snp.makeConstraints { make in
			make.top.equalTo(view.safeAreaLayoutGuide).offset(Constants.gradientLayerHeight - CreateProfileStackView.Constants.textFieldHeight - CreateProfileStackView.Constants.stackViewTopOffset)
			make.leading.equalToSuperview().offset(20)
			make.trailing.equalToSuperview().inset(20)
		}
		
		avatarView.snp.makeConstraints { make in
			make.centerX.equalToSuperview()
			make.height.width.equalTo(94)
			make.top.equalTo(view.safeAreaLayoutGuide)
		}
		
		nextButton.snp.makeConstraints { make in
			make.trailing.equalTo(textFieldStackView).offset(-20)
			make.bottom.equalTo(view.safeAreaLayoutGuide).offset(-20)
			make.width.height.equalTo(Constants.buttonWidth)
		}
	}
}
