//
//  ProfileStackView.swift
//  food_link
//
//  Created by Ramazan Kazybek on 5/7/21.
//

import FDUIKit
import UIKit
import SkyFloatingLabelTextField

protocol CreateProfileStackViewDelegate: AnyObject {
	/// Срабатывает когда пользователь нажимает на правую кнопку
	func emailTextField(_ text: String)
	/// Срабатывает когда пользователь нажимает на правую кнопку
	func nameTextField(_ text: String)
	/// Срабатывает когда пользователь нажимает на правую кнопку
	func addressTextField(_ text: String)
	/// Срабатывает когда пользователь нажимает на правую кнопку
	func phoneNumberTextField(_ text: String)
}

final class CreateProfileStackView: UIView {
	enum Constants {
		static let textFieldHeight: CGFloat = 35
		static let stackViewTopOffset: CGFloat = 20
	}
	
	weak var delegate: CreateProfileStackViewDelegate?
	
	private let stackView: UIStackView = {
		let stackView = UIStackView()
		stackView.distribution = .equalSpacing
		stackView.spacing = 25
		stackView.axis = .vertical
		return stackView
	}()
	
	private let containerView = UIView()
	
	private let nameTextField: UITextField =
		UITextFieldFactory(placeholder: "Имя",textField: RightButtonTextField())
		.tint(color: .appBlack)
		.font(FontFamily.Roboto.regular.font(size: 14))
		.setLeftImage(image: .init(imageLiteralResourceName: "name"))
		.setButtonImage(image: #imageLiteral(resourceName: "right_tick"), resignImage: .init(#imageLiteral(resourceName: "edit")))
		.build()
	
	private let surnameTextField: UITextField =
		UITextFieldFactory(placeholder: "Фамилия", textField: RightButtonTextField())
		.tint(color: .appBlack)
		.font(FontFamily.Roboto.regular.font(size: 14))
		.text(color: .appBlack)
		.setLeftImage(image: .init(imageLiteralResourceName: "surname"))
		.setButtonImage(image: #imageLiteral(resourceName: "right_tick"), resignImage: .init(#imageLiteral(resourceName: "edit")))
		.build()
	
	private let addressTextField: UITextField =
		UITextFieldFactory(placeholder: "Адрес",textField: RightButtonTextField())
		.tint(color: .appBlack)
		.text(color: .appBlack)
		.setLeftImage(image: .init(imageLiteralResourceName: "geo_marker"))
		.setButtonImage(image: #imageLiteral(resourceName: "right_tick"), resignImage: .init(#imageLiteral(resourceName: "edit")))
		.isOptional(true)
		.build()
	
	private lazy var phoneNumberTextField: TappableTextField = {
		let textField = TappableTextField(leftImage: #imageLiteral(resourceName: "visa"))
		textField.addTarget(self, action: #selector(tapped), for: .touchUpInside)
		textField.setTitle("Способ оплаты", for: .normal)
		return textField
	}()
	
	private let nonOptionalLabel: UILabel =
		UILabelFactory(text: "* Необязательные поля")
		.font(FontFamily.Roboto.regular.font(size: 14))
		.text(alignment: .center)
		.text(color: .appGray)
		.build()
	
	private let textFields: [UITextField]
	
	init() {
		textFields = [nameTextField, surnameTextField, addressTextField]
		super.init(frame: .zero)
		setupViews()
		textFields.forEach { $0.delegate = self }
	}
	
	required init(coder: NSCoder) {
		fatalError()
	}
	
	@objc
	func tapped() {
		print("text field tapped")
	}
	
	private func setupViews() {
		layer.cornerRadius = 20
		backgroundColor = .white
		addSubview(stackView)
		stackView.snp.makeConstraints { make in
			make.leading.equalToSuperview().offset(20)
			make.trailing.equalToSuperview().inset(20)
			make.top.equalToSuperview().offset(Constants.stackViewTopOffset)
			make.bottom.equalToSuperview().inset(10)
		}
		
		[nameTextField, surnameTextField, addressTextField, phoneNumberTextField, nonOptionalLabel].forEach { stackView.addArrangedSubview($0) }
		[nameTextField, surnameTextField, addressTextField, phoneNumberTextField].forEach { textField in
			textField.snp.makeConstraints { make in
				make.height.equalTo(Constants.textFieldHeight)
			}
		}
		
		nonOptionalLabel.snp.makeConstraints { make in
			make.height.equalTo(17)
		}
	}
}

extension CreateProfileStackView: UITextFieldDelegate {
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		guard let index = textFields.firstIndex(of: textField), textFields.count - 1 > index else {
			textField.resignFirstResponder()
			return true
		}
		textFields[index.advanced(by: 1)].becomeFirstResponder()
		return true
	}
}
