//
//  TappableTextField.swift
//  food_link
//
//  Created by Ramazan Kazybek on 5/11/21.
//

import FDUIKit

final class TappableTextField: UIButton {
	private let leftImage: UIImage?
	
	private lazy var textField: RegularTextField =
		UITextFieldFactory(textField: RightButtonTextField())
		.tint(color: .appBlack)
		.font(FontFamily.Roboto.regular.font(size: 14))
		.text(color: .appBlack)
		.setLeftImage(image: leftImage)
		.setButtonImage(image: #imageLiteral(resourceName: "right-chevron"))
		.build()
	
	init(leftImage: UIImage? = nil) {
		self.leftImage = leftImage
		super.init(frame: .zero)
		textField.isEnabled = false
		setupViews()
		setupConstraints()
	}
	
	required init?(coder: NSCoder) {
		nil
	}
	
	func setLeftImage(image: UIImage?) {
		textField.setLeftImage(image: image)
	}
	
	private func setupViews() {
		addSubview(textField)
	}
	
	private func setupConstraints() {
		textField.snp.makeConstraints { make in
			make.edges.equalToSuperview()
		}
	}
	
	override func setTitle(_ title: String?, for state: UIControl.State) {
		textField.placeholder = title
	}
	
	override func setImage(_ image: UIImage?, for state: UIControl.State) {
		guard let textField = textField as? RightButtonTextField else { return }
		textField.setButtonImage(image: image)
	}
}
