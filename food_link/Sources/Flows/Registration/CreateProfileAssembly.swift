//
//  ProfileAssembly.swift
//  food_link
//
//  Created by Ramazan Kazybek on 5/7/21.
//

import EasyDi

final class CreateProfileAssembly: Assembly {
	func makeCreateProfile() -> UIViewController {
		return define(init: CreateProfileViewController()) {
			return $0
		}
	}
}
