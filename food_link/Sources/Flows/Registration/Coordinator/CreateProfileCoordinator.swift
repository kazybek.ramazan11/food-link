//
//  ProfileCoordinator.swift
//  food_link
//
//  Created by Ramazan Kazybek on 5/7/21.
//

import FDUIKit
import EasyDi

public final class CreateProfileCoordinator: BaseCoordinator {
	public override init(router: Router) {
		super.init(router: router)
	}
	
	public override func start() {
		showCreateProfile()
	}
	
	private func showCreateProfile() {
		let assembly: CreateProfileAssembly = DIContext.defaultInstance.assembly()
		let controller = assembly.makeCreateProfile()
		router.setRootModule(controller)
	}
	
	private func showProfile() {
		let assembly: ProfileAssembly = DIContext.defaultInstance.assembly()
		let controller = assembly.makeProfile()
		router.push(controller)
	}
}
