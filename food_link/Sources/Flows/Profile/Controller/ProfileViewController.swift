//
//  ProfileViewController.swift
//  food_link
//
//  Created by Ramazan Kazybek on 5/12/21.
//

import FDUIKit

protocol ProfileViewPresenter: AnyObject {
	func display(items: [[ProfileSelectionCellViewModel]], sections: [String?])
}

final class ProfileViewController: UIViewController {
	private let avatarHeaderView = ProfileAvatarImageView()
	
	private let headerContainerView = UIView()
	
	private var items = [[ProfileSelectionCellViewModel]]()
	private var sections = [String?]()
	
	private let interactor: ProfileInteractor
	
	private lazy var tableView: UITableView = {
		let tableView = UITableView(frame: .zero, style: .grouped)
		tableView.delegate = self
		tableView.dataSource = self
		tableView.register(ProfileSelectionTableViewCell.self)
		tableView.alwaysBounceHorizontal = false
		tableView.separatorStyle = .none
		tableView.backgroundColor = .appBackground
		return tableView
	}()
	
	init(interactor: ProfileInteractor) {
		self.interactor = interactor
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		nil
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setupViews()
		setupConstraints()
		interactor.makeItems()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		setupNavBar()
	}
	
	private func setupNavBar() {
		navigationController?.navigationBar.isHidden = true
	}
	
	private func setupViews() {
		view.backgroundColor = .wildSand
		setupHeader()
		view.addSubview(tableView)
	}
	
	private func setupHeader() {
		headerContainerView.backgroundColor = .appOrange
		view.addSubview(headerContainerView)
		headerContainerView.addSubview(avatarHeaderView)
	}
	
	private func setupConstraints() {
		headerContainerView.snp.makeConstraints { make in
			make.top.leading.trailing.equalToSuperview()
		}
		
		avatarHeaderView.snp.makeConstraints { make in
			make.top.equalTo(view.safeAreaLayoutGuide)
			make.leading.equalToSuperview().offset(12)
			make.trailing.equalToSuperview().offset(-12)
			make.bottom.equalToSuperview().offset(-16)
		}
		
		tableView.snp.makeConstraints { make in
			make.top.equalTo(headerContainerView.snp.bottom)
			make.leading.trailing.bottom.equalToSuperview()
		}
	}
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return items[section].count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		return constructTableViewCell(tableView, cellForRowAt: indexPath, item: items[indexPath.section][indexPath.row])
	}
	
	private func constructTableViewCell<T>(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, item: T) -> UITableViewCell where T: ITableViewCellViewModel {
		let cell: T.Cell = tableView.dequeueReusableCell(for: indexPath)
		cell.configure(with: item)
		return cell
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		guard let text = sections[section] else { return nil }
		let view = ProfileSectionHeaderView(title: text)
		return view
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return sections.count
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		guard sections[section] != nil else { return .zero }
		return 40
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 60
	}
}

extension ProfileViewController: ProfileViewPresenter {
	func display(items: [[ProfileSelectionCellViewModel]], sections: [String?]) {
		self.items = items
		self.sections = sections
		
		tableView.reloadData()
	}
}
