//
//  ProfileAssembly.swift
//  food_link
//
//  Created by Ramazan Kazybek on 5/12/21.
//

import EasyDi

final class ProfileAssembly: Assembly {
	func makeProfile() -> UIViewController {
		let interactor = ProfileInteractor()
		let controller = ProfileViewController(interactor: interactor)
		return define(init: controller) {
			interactor.controller = $0
			return $0
		}
	}
}
