//
//  ProfileAvatarImageView.swift
//  food_link
//
//  Created by Ramazan Kazybek on 5/8/21.
//

import UIKit
import FDUIKit
import Kingfisher

final class ProfileAvatarImageView: UIView {
	private let avatarView = AvatarImageView()
	
	private let titleLabel: UILabel =
		UILabelFactory(text: "Ramazan Kazybek", style: .normal)
		.text(color: .white)
		.numberOf(lines: 0)
		.font(FontFamily.Roboto.bold.font(size: 24))
		.build()
	
	private let editButton: UIButton =
		UIButtonFactory(title: "Редактировать профиль", style: .underlined, color: .white)
		.font(FontFamily.Roboto.regular.font(size: 14))
		.build()
	
	init() {
		super.init(frame: .zero)
		setupViews()
		setupConstraints()
	}
	
	required init?(coder: NSCoder) {
		nil
	}
	
	private func setupViews() {
		avatarView.border(color: .appGreen)
		backgroundColor = .appOrange
		[avatarView, editButton, titleLabel].forEach { addSubview($0) }
	}
	
	private func setupConstraints() {
		avatarView.snp.makeConstraints { make in
			make.top.equalToSuperview()
			make.leading.equalToSuperview()
			make.width.height.equalTo(75)
			make.bottom.equalToSuperview()
		}
		
		editButton.snp.makeConstraints { make in
			make.centerY.equalTo(avatarView).offset(16)
			make.leading.equalTo(avatarView.snp.trailing).offset(12)
			make.trailing.lessThanOrEqualToSuperview()
		}
		
		titleLabel.snp.makeConstraints { make in
			make.centerY.equalTo(avatarView).offset(-16)
			make.leading.equalTo(avatarView.snp.trailing).offset(12)
			make.trailing.lessThanOrEqualToSuperview()
		}
	}
	
	private func configure(image: UIImage, title: String?) {
		titleLabel.text = title
		avatarView.setImage(image)
	}
	
	private func configure(url: URL, title: String?) {
		titleLabel.text = title
		avatarView.setImage(url)
	}
}
