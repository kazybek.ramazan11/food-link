//
//  ProfileSectionHeaderView.swift
//  food_link
//
//  Created by Ramazan Kazybek on 5/12/21.
//

import FDUIKit

final class ProfileSectionHeaderView: UIView {
	private let title: String
	
	private lazy var titleLabel: UILabel =
		UILabelFactory(text: title)
		.text(color: .appGray)
		.font(FontFamily.Roboto.bold.font(size: 15))
		.build()
	
	init(title: String) {
		self.title = title
		super.init(frame: .zero)
		setupViews()
		setupConstraints()
	}
	
	required init?(coder: NSCoder) {
		nil
	}
	
	private func setupViews() {
		addSubview(titleLabel)
	}
	
	private func setupConstraints() {
		titleLabel.snp.makeConstraints { make in
			make.bottom.equalToSuperview().inset(10)
			make.leading.equalToSuperview().offset(24)
			make.trailing.equalToSuperview().offset(-24)
		}
	}
}
