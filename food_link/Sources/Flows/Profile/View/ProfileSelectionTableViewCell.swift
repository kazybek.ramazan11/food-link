//
//  ProfileSelectionTableViewCell.swift
//  food_link
//
//  Created by Ramazan Kazybek on 5/12/21.
//

import UIKit

protocol ConfigurableTableViewCell: UITableViewCell {
	func configure(with viewModel: AnyObject)
}

final class ProfileSelectionTableViewCell: UITableViewCell {
	private weak var viewModel: ProfileSelectionCellViewModel?
	
	private let customSelectedBackgroundView = UIView()
	
	private lazy var tappableTextField: TappableTextField = {
		let textField = TappableTextField()
		textField.addTarget(self, action: #selector(cellTapped), for: .touchUpInside)
		return textField
	}()
		
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		selectedBackgroundView = customSelectedBackgroundView
		setupViews()
		setupConstraints()
	}
	
	required init?(coder: NSCoder) {
		nil
	}
	
	private func setupViews() {
		addSubview(tappableTextField)
	}
	
	private func setupConstraints() {
		tappableTextField.snp.makeConstraints { make in
			make.leading.equalToSuperview().offset(24)
			make.trailing.equalToSuperview().offset(-24)
			make.top.equalToSuperview().offset(20)
			make.bottom.equalToSuperview()
		}
	}
	
	@objc
	private func cellTapped() {
		viewModel?.actionBlock?()
	}
}

extension ProfileSelectionTableViewCell: ConfigurableTableViewCell {
	func configure(with viewModel: AnyObject) {
		guard let viewModel = viewModel as? ProfileSelectionCellViewModel else { return }
		self.viewModel = viewModel
		tappableTextField.setLeftImage(image: viewModel.image)
		tappableTextField.setTitle(viewModel.title, for: .normal)
	}
}

protocol ITableViewCellViewModel: AnyObject {
	associatedtype Cell: ConfigurableTableViewCell
}

class ProfileSelectionCellViewModel: ITableViewCellViewModel {
	typealias Cell = ProfileSelectionTableViewCell
	let image: UIImage?
	let title: String?
	
	var actionBlock: (() -> Void)?
	
	init(image: UIImage?, title: String?) {
		self.image = image
		self.title = title
	}
}


