//
//  ProfileInteractor.swift
//  food_link
//
//  Created by Ramazan Kazybek on 5/12/21.
//

final class ProfileInteractor {
	weak var controller: ProfileViewPresenter?
	
	private let items: [[ProfileSelectionCellViewModel]]
	
	private let sections: [String?]
	
	init() {
		
		items = [
			[
				.init(image: #imageLiteral(resourceName: "visa"), title: "**** 1111"),
				.init(image: #imageLiteral(resourceName: "geo_marker"), title: "Адрес"),
				.init(image: #imageLiteral(resourceName: "transaction_history"), title: "История транзакции")
			],
			[
				.init(image: #imageLiteral(resourceName: "contact_us"), title: "Связаться с нами"),
				.init(image: #imageLiteral(resourceName: "information"), title: "О нас"),
				.init(image: #imageLiteral(resourceName: "q_and_a"), title: "Вопросы и ответы")
			]
		]
		
		sections = [nil, "Информация"]
	}
	
	func makeItems() {
		controller?.display(items: items, sections: sections)
	}
}
