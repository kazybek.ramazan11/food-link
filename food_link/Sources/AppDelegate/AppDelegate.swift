//
//  AppDelegate.swift
//  food_link
//
//  Created by Ramazan Kazybek on 3/14/21.
//
//

import UIKit
import FDUIKit
import Authorization

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
	var window: UIWindow?
	private let configurator = AppDelegateConfiguratorFactory.makeDefault()
	private var applicationCoordinator: Coordinator?
	
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		let rootController = UINavigationController(rootViewController: UIViewController())
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = rootController
		applicationCoordinator = ApplicationCoordinator(router: Router(rootController: rootController))
		applicationCoordinator?.start()
		_ = configurator.application?(application, didFinishLaunchingWithOptions: launchOptions)
        window?.makeKeyAndVisible()
		_ = configurator.application?(application, didFinishLaunchingWithOptions: launchOptions)
        return true
    }
}
