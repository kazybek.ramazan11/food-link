//
//  TabBarCoordinatorFactory.swift
//  food_link
//
//  Created by iBol on 5/10/21.
//

import FDUIKit
import FDStock
import FDOrders
import FDCooks

final class TabBarCoordinatorFactory {
	func makeStock() -> (coordinator: Coordinator, module: Presentable) {
		let image = Asset.stockTabImageAsset.image
		let selectedImage = Asset.stockTabSelectedImageAsset.image
		let rootController = makeNavigation(with: .init(title: "Биржа", image: image, selectedImage: selectedImage))
		let coordinator = StockCoordinator(router: Router(rootController: rootController))
		return (coordinator, rootController)
	}
	
	func makeCooks() -> (coordinator: Coordinator, module: Presentable) {
		let image = Asset.cooksTabImageAsset.image
		let selectedImage = Asset.cooksTabSelectedImageAsset.image
		let rootController = makeNavigation(with: .init(title: "Повара", image: image, selectedImage: selectedImage))
		let coordinator = CooksCoordinator(router: Router(rootController: rootController))
		return (coordinator, rootController)
	}
	
	func makeOrders() -> (coordinator: Coordinator, module: Presentable) {
		let image = Asset.ordersTabImageAsset.image
		let selectedImage = Asset.ordersTabSelectedImageAsset.image
		let rootController = makeNavigation(with: .init(title: "Заказы", image: image, selectedImage: selectedImage))
		let coordinator = OrdersCoordinator(router: Router(rootController: rootController))
		return (coordinator, rootController)
	}
	
	private func makeNavigation(with style: UITabBarItem) -> UINavigationController {
		let navigationController = UINavigationController()
		navigationController.tabBarItem = style
		return navigationController
	}
}
