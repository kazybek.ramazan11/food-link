//
//  TabBarController.swift
//  food_link
//
//  Created by iBol on 5/10/21.
//

import UIKit
import FDUIKit
import EasyDi

public protocol TabBarPresentable: Presentable {
	func setViewControllers(_ viewControllers: [UIViewController])
	func setSelectedTab(_ tab: TabControllerType)
	
	var presentedTab: TabControllerType { get }
}

public final class TabBarController: UITabBarController, TabBarPresentable {
	// MARK: - Getters
	private(set) var selectedTab: Int = 0
	
	public var presentedTab: TabControllerType {
		TabControllerType(rawValue: selectedTab) ?? .cooks
	}
	// MARK: - Lifecycle

	override public func viewDidLoad() {
		super.viewDidLoad()
		tabBar.tintColor = .appOrange
		tabBar.unselectedItemTintColor = .gray
		tabBar.backgroundImage = UIImage()
		tabBar.backgroundColor = .white
		configureTabBarShadow()
	}

	private func configureTabBarShadow() {
		tabBar.shadowImage = UIImage()
		tabBar.layer.shadowRadius = 8
		tabBar.layer.shadowColor = UIColor.black.cgColor
		tabBar.layer.shadowOpacity = 0.03
	}

	public func setViewControllers(_ viewControllers: [UIViewController]) {
		self.viewControllers = viewControllers
	}
	
	public func setSelectedTab(_ tab: TabControllerType) {
		selectedIndex = tab.rawValue
		selectedTab = selectedIndex
	}
	
	override public func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
		selectedTab = selectedIndex
	}
}

// MARK: - TabControllerType
public enum TabControllerType: Int {
	case cooks, stock, orders
}
