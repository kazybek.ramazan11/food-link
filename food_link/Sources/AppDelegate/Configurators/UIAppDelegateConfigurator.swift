//
//  UIAppDelegateConfigurator.swift
//  Sberbank
//
//  Created by Ramazan Kazybek on 11/24/20.
//  Copyright © 2020 Sberbank Kazakhstan. All rights reserved.
//

import UIKit

final class UIAppDelegateConfigurator: AppDelegateConfigurator {
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		configureAppearance()
		return true
	}
	
	// MARK: - Appearance
	func configureAppearance() {
		UITextField.appearance().tintColor = .appOrange
	}
}
