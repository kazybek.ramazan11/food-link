//
//  ThirdPartiesAppDelegateConfigurator.swift
//  Sberbank
//
//  Created by Ramazan Kazybek on 11/24/20.
//  Copyright © 2020 Sberbank Kazakhstan. All rights reserved.
//

import UIKit
import Kingfisher
import IQKeyboardManager

final class ThirdPartiesAppDelegateConfigurator: AppDelegateConfigurator {
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		configureIQKeyboard()
		setupKingfisher()
		return true
	}
	
	// MARK: - IQKeyboardManager
	func configureIQKeyboard() {
		IQKeyboardManager.shared().isEnabled = true
		IQKeyboardManager.shared().isEnableAutoToolbar = true
		IQKeyboardManager.shared().shouldResignOnTouchOutside = true
	}
	
	private func setupKingfisher() {
		let cache = ImageCache.default
		cache.memoryStorage.config.expiration = .seconds(30)
		cache.diskStorage.config.expiration = .days(2)
		cache.memoryStorage.config.totalCostLimit = getMB(10)
	}
	
	private func getMB(_ value: Int) -> Int {
		return value * 1024 * 1024
	}
}
