//
//  StartupAppDelegateConfigurator.swift
//  Sberbank
//
//  Created by Ramazan Kazybek on 11/24/20.
//  Copyright © 2020 Sberbank Kazakhstan. All rights reserved.
//

import FDUIKit

final class StartupAppDelegateConfigurator: AppDelegateConfigurator {
	private var applicationCoordinator: ApplicationCoordinator?

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		guard let rootController = application.windows.first?.rootViewController as? UINavigationController else {
			fatalError("rootViewController must be CoordinatorNavigationController")
		}
		applicationCoordinator = ApplicationCoordinator(router: Router(rootController: rootController))
		applicationCoordinator?.start()
		return true
	}
}
