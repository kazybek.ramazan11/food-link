//
//  AppDelegateConfiguratorFactory.swift
//  Sberbank
//
//  Created by Ramazan Kazybek on 11/24/20.
//  Copyright © 2020 Sberbank Kazakhstan. All rights reserved.
//

enum AppDelegateConfiguratorFactory {
	static func makeDefault() -> AppDelegateConfigurator {
		let configurators: [AppDelegateConfigurator] = [
			StartupAppDelegateConfigurator(),
			ThirdPartiesAppDelegateConfigurator(),
			UIAppDelegateConfigurator()
		]
		return CompositeAppDelegateConfigurator(configurators: configurators)
	}
}
