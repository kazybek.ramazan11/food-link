//
//  ApplicationLaunchInstructor.swift
//  Sberbank
//
//  Created by Ramazan Kazybek on 11/24/20.
//  Copyright © 2020 Sberbank Kazakhstan. All rights reserved.
//

import FDNetworkLayer
import EasyDi

enum ApplicationLaunchInstruction {
	case unauthorized
	case pinCheck
}

final class ApplicationLaunchInstructor {
	private let networkAssembly: NetworkAssembly = DIContext.defaultInstance.assembly()
	
	var flow: ApplicationLaunchInstruction {
		return networkAssembly.isUserAuthorized() ? .pinCheck : .unauthorized
	}
}
