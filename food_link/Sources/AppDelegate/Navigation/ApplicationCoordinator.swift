//
//  ApplicationCoordinator.swift
//  Sberbank
//
//  Created by Ramazan Kazybek on 11/24/20.
//  Copyright © 2020 Sberbank Kazakhstan. All rights reserved.
//

import FDUIKit
//import Authorization

final class ApplicationCoordinator: BaseCoordinator {
	private let launchInstructor: ApplicationLaunchInstructor

	override init(router: Router) {
		launchInstructor = ApplicationLaunchInstructor()
		super.init(router: router)
	}

	override func start() {
		let coordinator = CreateProfileCoordinator(router: router)
		addDependency(coordinator)
		coordinator.start()
//		switch launchInstructor.flow {
//		case .unauthorized:
//			runAuthFlow()
//		case .pinCheck:
//			runPinCheckFlow()
//        }
//		runMainFlow()
	}

//	private func runAuthFlow() {
//		let coordinator: CoordinatorOutput = MainCoordinator(router: router)
//		coordinator.exitCompletion = { [weak self, weak coordinator] in
//			guard let self = self else { return }
//			self.removeDependency(coordinator)
//			self.runMainFlow()
//		}
//		addDependency(coordinator)
//		coordinator.start()
//	}
//
//	private func runPinCheckFlow() {
//		let coordinator: CoordinatorOutput = PinCreateCoordinator(router: router)
//		coordinator.exitCompletion = { [weak self, weak coordinator] in
//			guard let self = self else { return }
//			self.removeDependency(coordinator)
//			self.runMainFlow()
//		}
//		addDependency(coordinator)
//		coordinator.start()
//	}
//
//	private func runMainFlow() {
//		clearChildCoordinators()
//		let coordinator = TabBarCoordinator(router: router)
//		addDependency(coordinator)
//		coordinator.start()
//	}
}
