//
//  TabBarCoordinator.swift
//  food_link
//
//  Created by iBol on 5/10/21.
//

import Foundation
import FDUIKit
import KeyValueStorage
import StorageKit
import FDCooks
import FDStock
import FDOrders

public protocol ITabBarCoordinator: Coordinator {}

public final class TabBarCoordinator: BaseCoordinator, ITabBarCoordinator {
	private let tabBarController: TabBarPresentable
	private var cooksCoordinator: ICooksCoordinatorOutput?
	private var stockCoordinator: IStockCoordinatorOutput?
	private var ordersCoordinator: IOrdersCoordinatorOutput?
	
	private var viewControllers: [UIViewController] = []
	
	private let coordinatorFactory: TabBarCoordinatorFactory
	
	override public init(router: Router) {
		coordinatorFactory = TabBarCoordinatorFactory()
		tabBarController = TabBarController()
		super.init(router: router)
	}
	
	override public func start() {
		setupAllFlows()
		tabBarController.setViewControllers(viewControllers)
		router.setRootModule(tabBarController, isNavigationBarHidden: true)
	}
	
	private func setupAllFlows() {
		setupCooksFlow()
		setupStockFlow()
		setupOrdersFlow()
	}
	
	private func setupStockFlow() {
		let (coordinator, rootController) = coordinatorFactory.makeStock()
		if let coordinator = coordinator as? IStockCoordinatorOutput {
			stockCoordinator = coordinator
		}
		runFlow(coordinator: coordinator, rootController: rootController)
	}
	
	private func setupCooksFlow() {
		let (coordinator, rootController) = coordinatorFactory.makeCooks()
		if let coordinator = coordinator as? ICooksCoordinatorOutput {
			cooksCoordinator = coordinator
		}
		runFlow(coordinator: coordinator, rootController: rootController)
	}
	
	private func setupOrdersFlow() {
		let (coordinator, rootController) = coordinatorFactory.makeOrders()
		if let coordinator = coordinator as? IOrdersCoordinatorOutput {
			ordersCoordinator = coordinator
		}
		runFlow(coordinator: coordinator, rootController: rootController)
	}
	
	private func runFlow(coordinator: Coordinator, rootController: Presentable) {
		guard let controller = rootController.toPresent() as? UINavigationController  else { return }
		viewControllers.append(controller)
		addDependency(coordinator)
		coordinator.start()
	}
}
